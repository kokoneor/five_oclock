@extends('layouts.main')
@section('content')
    <div class="cabinet-page">
        <div class="container">
            <h1>Личный кабинет</h1>
            <div class="breadcrumbs">
                <a href="#" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Личный кабинет</p>
            </div>
            <div class="cabinet-grid grid grid-3">
                <div class="cabinet-position">
                    <div class="cabinet-tabs">
                        <div class="cabinet-tab active">
                            <p><i class="icon-user"></i> <span>Персональная информация</span></p>
                        </div>
                        <div class="cabinet-tab">
                            <p><i class="icon-bonus"></i><span>Бонусы</span></p>
                        </div>
                        <div class="cabinet-tab">
                            <p><i class="icon-card"></i><span>Покупки</span></p>
                        </div>
                        <div class="cabinet-tab">
                            <p><i class="icon-password"></i><span>Изменить пароль</span></p>
                        </div>
                        <div class="cabinet-tab">
                            <form method="post" action="/logout">
                                @csrf
                                <button class="authorization-btn">
                                    <p><i class="icon-password"></i><span>Выйти</span></p>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="cabinet-content">
                    <div class="cabinet-details">
                        <span>Детали аккаунта</span>
                        <img src="/img/user-main.jpg" alt="">
                    </div>
                    <div class="cabinet-form">
                        <form name="profileUpdateForm" id="profileUpdateForm">
                            <input type="hidden" name="user_id" id="userId" value="{{ $user->id }}">

                            <input class="cabinet-input" type="text" name="name" id="name" placeholder="Имя"
                            value="{{ $user->name }}">

                            <input class="cabinet-input" type="text" name="surname" id="lastName"
                                placeholder="Фамилия" value="{{ $user->surname }}">

                            <input class="cabinet-input" type="date" id="date" name="birthday"
                                   placeholder="Дата рождения" value="{{ $user->birthday }}">
                            <button type="submit" class="btn btn-cabinet">Сохранить</button>
                        </form>
                    </div>
                </div>
                <div class="cabinet-content">
                    <div class="cabinet-details">
                        <span>Бонусы <span class="info-bonus">?</span>
                            <div class="cabinet-popup">
                                Бонусы накапливаются с каждой покупкой и в зависимости от вашего уровня,
                                с каждой покупкой
                                ваш уровень растет и с ним размер Cash-back
                            </div>
                        </span>
                        <div class="cabinet-bonus">
                            @if($bonus)
                                <?php $bonusInfo = $bonus->getBonusInfo(); ?>
                                @if($bonusInfo)
                                    <div class="cabinet-bonus_top">
                                        <div class="cabinet-bonus_status">
                                            <p>Статус:</p>
                                            <p>{{ $bonusInfo->name }} {{ $bonusInfo->bonus }}%</p>
                                        </div>
                                        <div class="cabinet-bonus_level">
                                            <p>Ты достиг {{ $bonusInfo->name }}!
                                                Накоплено за все время:</p>
                                        </div>
                                        <div class="cabinet-bonus_money">
                                            <p>{{ $bonus->balance }} тг</p>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="cabinet-bonus_items">
                                @foreach($orders as $order)
                                    <div class="cabinet-bonus_item">
                                        <div class="cabinet-bonus_item-name">
                                            <p>Заказ {{ $order->id }}</p>
                                            <p>{{ $order->created_at }}</p>
                                        </div>
                                        <div class="cabinet-bonus_item-info">
                                            <div>
                                                @if($order->statuses)
                                                    <p>{{ $order->statuses->name }}</p>
                                                @endif

                                                @if(isset($bonus))
                                                    <p>{{ ($order->full_price * $bonus->bonus) / 100 }} Тг</p>
                                                @else
                                                    <p>{{ $order->full_price }} Тг</p>
                                                @endif
                                            </div>
                                            @if(!($order->pay_out_bonus == 1))
                                                <p>Ожидается подтверждение кэшбэка</p>
                                            @endif

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cabinet-content">
                    <span>Покупки</span>
                    <div class="purchases">
                        <div class="purchases-top">
                            <span>Номер заказа</span>
                            <span>Дата</span>
                            <span>Статус</span>
                            <span>Итого</span>
                            <span>Просмотр</span>
                        </div>
                        <div class="purchases-items">
                            @foreach($orders as $order)
                                <div class="purchases-item">
                                    <span>Заказ #{{ $order->id }}</span>
                                    <p>{{ $order->created_at }}</p>
                                    @if($order->statuses)
                                        <h3 style="color: {{ $order->statuses->color }}">
                                            {{ $order->statuses->name }}
                                        </h3>
                                    @else
                                        <h3>Заказ оформлен</h3>
                                    @endif
                                    <p class="purchases-cost">{{ $order->full_price }} тг</p>
                                    @if($order->statuses)
                                        <a href="#" class="btn purchases-btn"
                                           data-id="{{ $order->id }}"
                                           data-user="{{ $user->id }}"
                                           data-orderStatus="{{ $order->statuses->name }}"
                                        >
                                    @else
                                        <a href="#" class="btn purchases-btn"
                                           data-id="{{ $order->id }}"
                                           data-user="{{ $user->id }}"
                                           data-orderStatus="Заказ оформлен"
                                        >
                                    @endif

                                        Посмотреть
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="cabinet-content">
                    <span>Изменить пароль</span>
                    <form name="updatePasswordForm" id="updatePasswordForm">
                        <div class="password-edit">
                            <input type="hidden" name="user_id" value="{{ $user->id }}">

                            <input type="password" name="password" placeholder="Новый пароль" class="cabinet-input" id="password">

                            <input type="password" name="password_confirmation" placeholder="Повторите пароль" class="cabinet-input" id="password-retry">

                            <button class="btn btn-cabinet password-edit_btn">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script
                src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.js" type="text/javascript"></script>
        <script src="/js/request.js"></script>

    </div>
@endsection
