<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Five oClock</title>
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="cooking-bg">
        <div id="cooking">
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div id="area">
                <div id="sides">
                    <div id="pan"></div>
                    <div id="handle"></div>
                </div>
                <div id="pancake">
                    <div id="pastry"></div>
                </div>
            </div>
        </div>
    </div>
    <header class="header">
        <nav class="nav">
            <div class="burger">
                <div class="burger-bg">
                    <div class="additional-burger">
                        <div class="top-bun"></div>
                        <div class="meat"></div>
                        <div class="bottom-bun"></div>
                        <div class="popup-menu">
                            <div class="popup-grid grid grid-3">
                                <div class="popup-block">
                                    <ul>
                                        <li><a href="#">СМОТРЕТЬ ВСЕ</a></li>
                                        <li><a href="#">Coffee</a></li>
                                        <li><a href="#">Кондитерка</a></li>
                                    </ul>
                                    <details>
                                        <summary>ВЫПЕЧКА</summary>
                                        <ul>
                                            <li><a href="#">Сдоба</a></li>
                                            <li><a href="#">Пироженное</a></li>
                                            <li><a href="#">Торты</a></li>
                                            <li><a href="#">Пироги</a></li>
                                            <li><a href="#">Шоколад</a></li>
                                            <li><a href="#">Рулеты</a></li>
                                        </ul>
                                    </details>
                                </div>
                                <div class="popup-block">
                                    <ul>
                                        <li><a href="#">Coffee</a></li>
                                        <li><a href="#">Кондитерка</a></li>
                                        <li><a href="#">Выпечка</a></li>
                                    </ul>
                                    <details>
                                        <summary>НАПИТКИ</summary>
                                        <ul>
                                            <li><a href="#">Coca Cola</a></li>
                                            <li><a href="#">Fanta</a></li>
                                            <li><a href="#">Milk</a></li>
                                        </ul>
                                    </details>
                                </div>
                                <div class="popup-block">
                                    <img src="img/banner.jpg" alt="Баннер">
                                    <h3>Coffee Five O’clock</h3>
                                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="logo">
                <img src="img/logo.svg" alt="Five oClock">
            </a>
            <ul class="nav__menu">
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Главная</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">О нас</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Кейтеринг</a></li>
                <li class="nav__menu-item mobile-list"><a href="#" class="nav__menu-link url-flex">Доп меню <i
                            class="icon-right"></i></a></li>
                <ul class="submenu">
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">СМОТРЕТЬ ВСЕ</a></li>
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">Coffee</a></li>
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">Кондитерка</a></li>
                    <li class="nav__menu-item mobile-list"><a href="#" class="nav__menu-link url-flex">Выпечка <i
                                class="icon-right"></i></a></li>
                    <ul class="submenu">
                        <li class="nav__menu-item"><a href="#" class="nav__menu-link">Cдоба</a></li>
                        <li class="nav__menu-item"><a href="#" class="nav__menu-link">Пироженные</a></li>
                        <li class="nav__menu-item"><a href="#" class="nav__menu-link">Торты</a></li>
                    </ul>
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">Контакты</a></li>
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">Личный кабинет</a></li>
                </ul>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Акции</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Каталог</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Мероприятия</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Контакты</a></li>
                <li class="nav__menu-item"><a href="#" class="nav__menu-link">Личный кабинет</a></li>
            </ul>
            <button class="btn btn-nav">Индивидуальный заказ</button>
            <button class="btn-nav mobile-hide"><i class="icon-telephone"></i></button>
            <div class="sign-up">
                <a href="#">
                    <i class="icon-signup"></i>
                </a>
            </div>
        </nav>
    </header>
    <div class="smart-page">
        <div class="container">
            <h1>Smart catering</h1>
            <div class="breadcrumbs smart-breadcrumbs">
                <a href="#" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Smart catering</p>
            </div>
            <div class="grid smart-info_grid">
                <div>
                    <p>Как организовать выездное или внутреннее мероприятие? Как это сделать быстро, вкусно,
                        профессионально? Как влезть в запланированный бюджет и правильно рассчитать вкус? Неважно:</p>
                </div>
                <div>
                    <img src="img/question.svg" alt="">
                </div>
            </div>
            <div class="catering-lists grid grid-3">
                <div>
                    <p>Будет это вечеринка на</p>
                    <div class="catering-list_number">
                        <span>10</span>
                        <span>+</span>
                    </div>
                </div>
                <div>
                    <p>Фуршет на</p>
                    <div class="catering-list_number">
                        <span>200</span>
                        <span>+</span>
                    </div>
                </div>
                <div>
                    <p>Масштабный корпоратив на</p>
                    <div class="catering-list_number">
                        <span>800</span>
                        <span>+</span>
                    </div>
                </div>
            </div>
            <div class="catering-sliders">
                <div class="catering-prev"><i class="icon-left"></i></div>
                <div class="catering-next"><i class="icon-right"></i></div>
                <div class="swiper catering-swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="catering-slider">
                                <h2>Присоединяйтесь к клубу Club Sandwich каждое утро!</h2>
                                <a href="#" class="btn btn-yellow btn-slider">В каталог</a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catering-slider">
                                <h2>Присоединяйтесь к клубу Club Sandwich каждое утро!</h2>
                                <a href="#" class="btn btn-yellow btn-slider">В каталог</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="catering-info">
            <div class="container">
                <div class="catering-info_grid grid grid-3">
                    <span>Вы уже хоть раз задумывались о проведении мероприятия в Алматы или Кызыл Орде, а значит
                        сталкивались
                        с понятием “кейтеринг”</span>
                    <p>Это - услуга выездного ресторанного обслуживания. В нее включена доставка блюд на мероприятие,
                        особая
                        сервировка (подача еды), профессиональные услуги обслуживающего персонала.</p>
                    <p>По сути - это ресторан на колесах, который оформит ваш заказ под ключ: приедет  с готовой едой,
                        привезет напитки, закуски или основные блюда в любом количестве,  в любое время и место.
                    </p>
                </div>
            </div>
        </div>
        <div class="quickly">
            <div class="container">
                <h2>Быстро, вкусно и посуду мыть не нужно</h2>
                <div class="quckly_grid grid grid-3">
                    <div class="quckly_card">
                        <h3>предзаказ</h3>
                        <p>На данный момент работаем в режиме предзаказа: <b>делай заказ сегодня и получай завтра</b>
                        </p>
                    </div>
                    <div class="quckly_card">
                        <h3>Экономия</h3>
                        <p>Вам не нужно готовить, выдумывать меню, мыть посуду</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Гибкость</h3>
                        <p>В нашем арсенале есть стандартное меню для всех видов мероприятий: <b>фуршета, корпоратива,
                                бизнес-обеда, коктейля, кофе-брейка </b>. Но мы привыкли быть гибкими и оформим ваш
                            заказ по
                            индивидуальному запросу: с учетом комфортной стоимости и числа гостей</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Качество</h3>
                        <p>Мы готовим все блюда из натуральных продуктов. Готовая еда уезжает к вам в офис, на
                            мероприятие или домой из-под ножа. <b>Smart Catering Five O'Clock</b> - это
                            свежеприготовленные
                            закуски и выпечка</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Профессионализм</h3>
                        <p> <b>Мы готовим всю еду самостоятельно </b> - от закусок до самых сложных кремов на вашем
                            торте. Сами
                            взбиваем соус Голландез, печем хлеб, жарим мясо и не только</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Smart Catering 5 O'Clock</h3>
                        <p>Это гармония качества, эстетики и приемлемой цены</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Цена</h3>
                        <p> <b>Smart Catering Five O'Clock</b> - ваш обед не отличить от ресторанного. Зато его
                            стоимость будет
                            более доступной</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ours-clients">
            <div class="container">
                <div class="ours-clients_content">
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum</p>
                    <a href="#" class="btn btn-yellow btn-slider">Наши клиенты</a>
                </div>
            </div>
        </div>
    </div>

    <section class="catering">
        <div class="container">
            <h2 class="title">Кейтеринг мероприятия</h2>
            <div class="catering-grid grid">
                <div class="photo_date">
                    <div class="main-event">
                        <img class="catering-mainBg" src="img/event-1.jpg" alt="текст">
                        <div class="additional-event">
                            <img class="additional-img" src="img/event-2.jpg" alt="">
                            <img class="additional-img" src="img/event-3.jpg" alt="">
                            <img class="additional-img" src="img/event-4.jpg" alt="">
                        </div>
                    </div>
                    <div class="event-date">
                        <div class="event-start">
                            <p class="date-subtitle">Дата начала:</p>
                            <div class="event-numbber">
                                <h3>01.07.2021 <span>года</span></h3>
                            </div>
                        </div>
                        <div class="event-end">
                            <p class="date-subtitle">Дата завершения:</p>
                            <div class="event-numbber">
                                <h3>18.07.2021 <span>года</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-content">
                    <h3 class="event-content-title">По своей сути рыбатекст является альтернативой традиционному</h3>
                    <p class="event-content-subtitle">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру
                        сгенерировать несколько абзацев</p>
                    <p class="event-content-descr">По своей сути рыбатекст является альтернативой традиционному lorem
                        ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии
                        от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст
                        неповторимый колорит советских времен.

                        Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных
                        выступлений в домашних условиях. При создании генератора мы использовали небезизвестный
                        универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти</p>
                    <a href="#" class="event-content-more link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                </div>
            </div>
            <div class="articles_grid grid grid-3">
                <div class="article">
                    <div class="article-img_info">
                        <img src="img/article-1.jpg" alt="Название статьи">
                        <div class="article-date">с 01.07.21 до 18.07.21</div>
                    </div>
                    <h3 class="article-name">По своей сути рыбатекст является альтернативой</h3>
                    <p class="article-descr">В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет
                        непонятным смыслом и придаст неповторимый колорит</p>
                    <a href="#" class="link article-link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                </div>
                <div class="article">
                    <div class="article-img_info">
                        <img src="img/article-2.jpg" alt="Название статьи">
                        <div class="article-date">с 01.07.21 до 18.07.21</div>
                    </div>
                    <h3 class="article-name">По своей сути рыбатекст является альтернативой</h3>
                    <p class="article-descr">В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет
                        непонятным смыслом и придаст неповторимый колорит</p>
                    <a href="#" class="link article-link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                </div>
                <div class="article">
                    <div class="article-img_info">
                        <img src="img/article-3.jpg" alt="Название статьи">
                        <div class="article-date">с 01.07.21 до 18.07.21</div>
                    </div>
                    <h3 class="article-name">По своей сути рыбатекст является альтернативой</h3>
                    <p class="article-descr">В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет
                        непонятным смыслом и придаст неповторимый колорит</p>
                    <a href="#" class="link article-link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="company">
        <div class="container">
            <div class="company-grid grid grid-3">
                <div class="company-block">
                    <h2 class="company-title">О нашей компании <span>Five O'Clock</span></h2>
                    <button class="btn btn-company">ПОДРОБНЕЕ</button>
                </div>
                <div class="company-block">
                    <p>Меню в <b>Five O'Clock</b> уделяют особое внимание. <b>Шеф-бариста</b> не терпит компромиссов и
                        может
                        запросто
                        отправить арабику обратно даже самому проверенному производителю зерен </p>
                    <p><b>Five O'Clock</b> – открытая кухня a la carte. Сидя в зале, можно наблюдать, как повара
                        раскатывают
                        домашнее тесто, делают из него пышные булочки или режут багеты, создают самые настоящие
                        кулинарные шедевры <b>"меню из под ножа"</b> континентальной кухни.

                    </p>
                </div>
                <div class="company-block">
                    <p>В дополнение к классической кофейной карте в <b>Five O'Clock</b> можно попробовать редчайшие
                        сорта чая,
                        вручную отобранные шеф-бариста <b>Five O'Clock</b> в разных уголках планеты.</p>
                    <p>В <b>Five O'Clock</b> рука об руку идут высокое качество продуктов и авторская подача. В нашем
                        меню вы
                        не найдете сложного меню, но вкус к эстетике подачи и мастерству <b>шеф-повара</b> запомнится
                        надолго.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="footer-grid grid grid-4">
                <div class="footer-block">
                    <img class="logo-white" src="img/logo-white.svg" alt="Лого">
                </div>
                <div class="footer-block">
                    <h3 class="footer-title">График работы</h3>
                    <div class="footer-text">
                        <p>Пн-Пт с 8:00 до 19:00</p>
                        <p>Сб с 8:00 до 17:00</p>
                        <p>Воскресенье выходной</p>
                    </div>
                </div>
                <div class="footer-block">
                    <h3 class="footer-title">Контакты</h3>
                    <div class="footer-text">
                        <p>Казахстан, Астана, пр. Ленинский 1</p>
                        <a href="tel:+77779876543">7 777 987 65 43</a>
                        <a href="mailto:info@fiveoclock.com">info@fiveoclock.com</a>
                    </div>
                </div>
                <div class="footer-social">
                    <div class="vk">
                        <i class="icon-vk"></i>
                        <p>Vkontakte</p>
                    </div>
                    <div class="fb">
                        <i class="icon-facebook"></i>
                        <p>Facebook</p>
                    </div>
                    <div class="instagram">
                        <i class="icon-instagram"></i>
                        <p>Instagram</p>
                    </div>
                </div>
            </div>
            <p class="footer-info">Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В
                то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem
                Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков,
                но и перешагнул в электронный дизайн.</p>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-content">
                    <p>Разработано "Shedry Studio"</p>
                    <p>© Все права защищены</p>
                    <a href="#" class="policy">Политика конфиденциальности</a>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="js/main.js"></script>
</body>
