@extends('layouts.main')
@section('content')
    <script>
        let quantity = 1;

        function plusItem() {
            quantity += 1;
            document.getElementById('item-quantity').innerText = quantity;
        }

        function minusItem() {
            if (quantity > 2) {
                quantity -= 1;
                document.getElementById('item-quantity').innerText = quantity;
            }
        }

    </script>
    <div class="card-page">
        <div class="container">
            <h1>Карточка товара</h1>
            <div class="breadcrumbs">
                <a href="/" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Карточка товара</p>
            </div>
            <div class="card-page_grid grid">
                <div class="card-page_photo">
                    <div class="card-page_mainPhoto">
                        <img class="mainPhoto_img" src="{{ asset('storage/' . $product->image) }}"
                             alt="Название товара">
                    </div>
                    <div class="card-page_additional-photos">
                        <div class="card-page_additional-photo">
                            <img class="additional-photo_img" src="img/cardPage-2.jpg" alt="">
                        </div>
                        <div class="card-page_additional-photo">
                            <img class="additional-photo_img" src="img/cardPage-3.jpg" alt="">
                        </div>
                        <div class="card-page_additional-photo">
                            <img class="additional-photo_img" src="img/cardPage-4.jpg" alt="">
                        </div>
                        <div class="card-page_additional-photo">
                            <img class="additional-photo_img" src="img/cardPage-5.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="card-page_info">
                    <h2 class="title">{{ $product->name }}</h2>
                    <div style="display:inline-block;">
                        @if($product->is_active == 1)
                            <p class="card-page_check" style="display: inline-block">
                                <i class="icon-check"></i> В наличии
                            </p>
                        @else
                            <p class="card-page_check uncount" style="display: inline-block">
                                <i class="icon-clear"></i> Нет наличии
                            </p>
                        @endif
                        @if($product->is_order == 1)
                            <p class="card-page_check card-order" style="display: inline-block; margin-left: 50px;">
                                <i class="icon-edit"></i> На заказ
                            </p>
                        @endif
                    </div>
                    <h3 class="card-page_cost">
                        {{ $product->price }} <span>тг</span> / {{ $product->demension }}
                    </h3>
{{--                    @foreach($variations as $variation)--}}
{{--                        <h3 class="card-page_cost">{{ $variation->price }} <span>тг</span></h3>--}}
{{--                        <div class="card-page_description">--}}
{{--                            <p>За {{ $variation->name }} </p>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
                    <div class="card-page_description">
                        <span>Описание</span>
                        <p>{{ $product->description }} </p>
                    </div>
                    <div class="card-page_ingredients">
                        @if(!$partials->isEmpty())
                            <span>Ингредиенты</span>
                        @endif
                        @foreach($partials as $ingredient)
                            <p>{{$ingredient->ingredient }} - {{$ingredient->dimension }}</p>
                        @endforeach
                    </div>
                    @if($product->is_active == 1)
                        <div class="card-page_block">
                            <div class="card-page_counts">
                                <p>Количество</p>
                                <div class="count-control">
                                    <button class="btn btn-yellow btn-count-modal" style="margin-right: 25px;"
                                            onclick="minusItem()">-
                                    </button>
                                    <span class="total-modal" id="item-quantity"></span>
                                    <button class="btn btn-count-modal" style="color: #fff; margin-left: 25px;"
                                            onclick="plusItem()">+
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-page_buttons">
                            <button class="btn btn-yellow card-btn btn-buy btn-card_page" data-id="{{ $product->id }}">
                                <i class="icon-money"></i>Купить
                            </button>
                            <button class="btn btn-card_page card-btn-cart"
                                    onclick="addToCart({{ $product->id }}, quantity)"><i class="icon-cart"></i>В корзину
                            </button>
                        </div>
                    @endif
                    @if($product->is_order == 1)
                        <button class="btn btn-yellow btn-order btn-card_page" data-id="{{ $product->id }}">
                            <i class="icon-edit"></i>Заказать
                        </button>
                    @endif
                </div>
            </div>
            <div class="similar">
                @if(!empty($similar_products))
                    <h2>Похожие товары</h2>
                    <div class="similar-grid grid grid-4">
                        @foreach($similar_products as $similar_product)
                            <div class="card-item">
                                <div class="card-item_content">
                                    <a href="/product/{{ $similar_product->id }}">
                                        <img src="{{ asset('storage/' . $similar_product->image) }}"
                                             alt="Название товара" class="card-item_img">
                                        <h3 class="card-item_name">{{ $similar_product->name }}</h3>
                                        <span>Ингредиенты:</span>
                                        @foreach($similar_product->partials as $ingredient)
                                            <p>{{$ingredient->ingredient }} - {{$ingredient->dimension }}</p>
                                        @endforeach
                                        <div class="card-item_cost">
                                            @foreach($similar_product->variations as $variation)
                                                <span>Цена за {{ $variation->name }}</span>
                                                <p>{{ $variation->price }} тг</p>
                                            @endforeach
                                        </div>
                                    </a>
                                </div>
                                <div class="card-item_buttons">
                                    <button class="btn card-btn btn-yellow"><i class="icon-money"></i>Купить</button>
                                    <button class="btn card-btn"><i class="icon-cart"></i>В корзину</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        document.getElementById('item-quantity').innerText = quantity;
    </script>

@endsection
