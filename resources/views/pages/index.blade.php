@extends('layouts.main')
@section('content')
    <section class="main">
        <div class="container">
            <div class="slides">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($banners as $banner)
                            <div class="swiper-slide"
                                 style="background: url({{ asset('storage/' . str_replace('\\','/', $banner->image)) }}) center center/cover no-repeat">
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>
            <h1 class="main-title">Современное городское кафе с <span>Характером Джентельмена</span></h1>
        </div>
        <div class="main-bottom grid grid-4 main-grid">
            <div class="in-catalog">
                <a href="/catalog"><button class="btn big-btn">ПЕРЕЙТИ В КАТАЛОГ</button></a>
                <div class="progress-slider">
                    <p class="current-slide">01</p>
                    <div id="progress" class="swiper-pagination swiper-progress-bar">
                        <div class="progress"></div>
                    </div>
                    <p class="total-slides">04</p>
                </div>
            </div>
            <div class="bottom-first-content">
                <h3 class="bottom-title">Five O'Clock:Cafeteriy</h3>
                <p class="bottom-descr">Шеф-пекарь, шеф-кондитер и шеф-бариста создали здесь совершенно новые и
                    запоминающиеся чудеса кулинарного искусства</p>
            </div>
            <div class="bottom-second-content">
                <p class="bottom-descr">Это идеальное место для проведения деловых встреч и неспешной работы вне
                    офиса. А можно зайти ненадолго – перекусить и выпить кофе с вкуснейшим свежим десертом</p>
            </div>
            <div class="bottom-banner">
                <img src="img/coffee.jpg" alt="Баннер">
            </div>
        </div>
    </section>
    <section class="category">
        <div class="container">
            <div class="category-top">
                <h2 class="title category-title">Категории</h2>
                <div class="category-arrows">
                    <a href="/catalog" style="margin-left: 840px">Все категории</a>
                </div>
            </div>
            <div class="category-grid grid">
                @foreach($catalog as $category)
                    @if($loop->index == 0)
                        <div class="category-item">
                            <div class="category-content">
                                <p class="category-subtitle">{{ $category->title }}</p>
                                <h3 class="category-title">{{ $category->name }}</h3>
                                <p class="category-descr">{{ $category->description }}</p>
                                <a href="/catalog" class="category-catalog">В каталог</a>
                            </div>
                        </div>
                    @elseif($loop->index == 1 || $loop->index == 2)
                        <div class="category-item" style="background-image: url({{ asset('storage/' . str_replace('\\','/', $category->image)) }});">
                            <div class="category-content">
                                <p class="category-subtitle">{{ $category->title }}</p>
                                <h3 class="category-title">{{ $category->name }}</h3>
                                <p class="category-descr">{{ $category->description }}</p>
                                <a href="/catalog" class="category-catalog">В каталог</a>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </section>
    <section class="new-items">
        <div class="container">
            <div class="new-items_top">
                <h2 class="title">Новинки</h2>
                <div class="tabs">
                    @foreach($mainTabs as $tab)
                        <div class="tab">{{ $tab->name }}</div>
                    @endforeach
                </div>
            </div>
            @foreach($mainTabs as $tab)
                <div class="tab-content">
                    <div class="new-items_grid grid grid-4">
                        @php
                            $products = $tab->getProducts();
                        @endphp
                        @foreach($products as $product)
                            <div class="card-item">
                                <div class="card-item_content">
                                    <a href="/product/{{ $product->id }}">
                                        <img src="{{ asset('storage/' . $product->image) }}" alt="Название товара" class="card-item_img">
                                        <h3 class="card-item_name">{{ $product->name }}</h3>
                                        @if(!$product->partials->isEmpty())
                                            <span>Ингредиенты</span>
                                        @endif
                                        <div class="ingredients">
                                            @foreach($product->partials as $ingredient)
                                                <p>{{$ingredient->ingredient }} - {{$ingredient->dimension }}</p>
                                            @endforeach
                                        </div>
                                        <div class="card-item_cost">
                                            <span>Цена за {{ $product->dimension }}</span>
                                            <p>{{ $product->price }} тг</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="card-item_buttons">
                                    <button class="btn card-btn btn-yellow" data-id="{{ $product->id }}">
                                        <i class="icon-money"></i>Купить
                                    </button>
                                    <button class="btn card-btn-cart" onclick="addToCart({{ $product->id }}, 1)"><i class="icon-cart"></i>В корзину</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section class="club">
        <div class="container">
            <div class="club-content">
                <h2 class="club-title">Five O'Clock</h2>
                <p>Проводит званые вечера, выездные ужины и гостевые вечеринки, которые объединяют всех гостей</p>
                <h3>Присоединяйтесь к клубу Club Sandwich каждое утро!</h3>
                <a href="/catalog" class="btn btn-yellow club-btn">В каталог</a>
            </div>
        </div>
    </section>
    <section class="catering">
        <div class="container">
            @foreach($events as $event)
                @if($event->is_main == 1 && $loop->first)
                <h2 class="title">Кейтеринг мероприятия</h2>
                <div class="catering-grid grid">
                    <div class="photo_date">
                        <div class="main-event">
                            <img class="catering-mainBg" src="{{ asset('storage/' . $event->image) }}" alt="текст">
                            <div class="additional-event">
                                <?php $images = json_decode($event->images); ?>
                                @if(!empty($images))
                                    @foreach($images as $image)
                                        <img class="additional-img" src="{{ asset('storage/' . $image) }}" alt="">
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="event-date">
                            <div class="event-start">
                                <p class="date-subtitle">Дата начала:</p>
                                <div class="event-numbber">
                                    <h3>{{ $event->date_start }} <span>года</span></h3>
                                </div>
                            </div>
                            <div class="event-end">
                                <p class="date-subtitle">Дата завершения:</p>
                                <div class="event-numbber">
                                    <h3>{{ $event->date_end }} <span>года</span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event-content">
                        <h3 class="event-content-title">{{ $event->title }}</h3>
                        <p class="event-content-subtitle">{{ $event->name }}</p>
                        <p class="event-content-descr">{{ $event->description }}</p>
                        <a href="{{ '/event/' . $event->id }}" class="event-content-more link">
                            ПОДРОБНЕЕ <i class="icon-right"></i>
                        </a>
                    </div>
                </div>
                @endif
            @endforeach



            <div class="articles_grid grid grid-3">
                @foreach($events as $event)
                    @if($event->is_main == 0)
                        <div class="article">
                            <div class="article-img_info">
                                <img src="{{ asset('storage/' . $event->image) }}" alt="Название статьи">
                                <div class="article-date">с {{ $event->date_start }} до {{ $event->date_end }}</div>
                            </div>
                            <h3 class="article-name">{{ $event->name }}</h3>
                            <p class="article-descr">{{ $event->description }}</p>
                            <a href="{{ '/event/' . $event->id }}" class="link article-link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </section>
    <section class="promo">
        <div class="container">
            <h2 class="title">Акции</h2>
            <div class="promo-grid grid grid-3">
                @foreach($sales as $sale)
                    <div class="promo-block">
                        <img src="{{ asset('storage/' . $sale->image) }}" alt="Промо">
                        <h3 class="promo-name">{{ $sale->name }}</h3>
                        <p class="promo-descr">{{ $sale->content }}</p>
                        <a href="{{ '/sale/' . $sale->id }}" class="promo-link link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                    </div>
                @endforeach
            </div>
            {{--<div class="pagination pagination-main">--}}
                {{--<div class="pagination-count active">--}}
                    {{--1--}}
                {{--</div>--}}
                {{--<div class="pagination-count">--}}
                    {{--2--}}
                {{--</div>--}}
                {{--<div class="pagination-count">--}}
                    {{--3--}}
                {{--</div>--}}
                {{--<div class="pagination-count disabled">--}}
                    {{--...--}}
                {{--</div>--}}
                {{--<div class="pagination-count circle">--}}
                    {{--<i class="icon-arrow"></i>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>
@endsection
