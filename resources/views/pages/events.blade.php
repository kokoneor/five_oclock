@extends('layouts.main')
@section('content')
    <div class="event-page">
        <div class="container">
            <h1>Мероприятие</h1>
            <div class="breadcrumbs event-breadcrumbs">
                <a href="#" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Мероприятие</p>
            </div>
            <div class="event_photos">
                <div class="events_screen">
                    <img class="catering-mainBg" src="{{ asset('storage/' . $event->image) }}" alt="">
                    <div class="event_small_photos">
                        <img class="additional-img" src="img/event-page_1.jpg" alt="">
                        <img class="additional-img" src="img/event-page_2.jpg" alt="">
                        <img class="additional-img" src="img/event-page_3.jpg" alt="">
                        <img class="additional-img" src="img/event-page_1.jpg" alt="">
                        <img class="additional-img" src="img/event-page_2.jpg" alt="">
                        <img class="additional-img" src="img/event-page_3.jpg" alt="">
                        <img class="additional-img" src="img/event-page_1.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="events-content">
                <div class="events-content_top">
                    <h2>{{ $event->title }}</h2>
                    <div class="events-content_date">
                        <div class="events-content_start">
                            <p>Дата начала:</p>
                            <h2>{{ $event->date_start }} <span>года</span></h2>
                        </div>
                        <div class="events-content_end">
                            <p>Дата завершения:</p>
                            <h2>{{ $event->date_end }} <span>года</span></h2>
                        </div>
                    </div>
                </div>
                <p class="events-subtitle">{{ $event->name }}</p>
                <p class="events-description">{!! $event->description !!}</p>
                @if($event->video)
                    <div class="video-event">
                        <iframe width="100%" height="600" src="{{ $event->video }}"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                @endif
                {!! $event->content !!}
            </div>
        </div>
    </div>
@endsection