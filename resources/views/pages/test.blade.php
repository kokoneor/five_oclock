@extends('layouts.main')
@section('content')
    <div class="modal-body">
        <form id="authorization-form" name="authorizationForm" class="login-form">
            @csrf
            <p class="input-title login-title">Электронная почта</p>
            <input id="email-login" required type="email" name="email" class="modal-input">

            <p class='input-title login-title'>Пароль</p>
            <input id="password-login" required type="password" name="password" class="modal-input">

            <div class="modal-bottom">
                <button type="submit" class="confirm-button btn">Войти</button>
                <p class="modal-or">или</p>
                <a href="#" class="register">Регистрация</a>
            </div>
        </form>
    </div>
@endsection

{{--<div class="modal-body">--}}
    {{--<form  class="login-form" method="POST" action="{{ route('login') }}">--}}
        {{--@csrf--}}
        {{--<p class="input-title login-title">Электронная почта</p>--}}
        {{--<input id="email-login" required type="email" name="email" class="modal-input">--}}
        {{--@error('email')--}}
        {{--<span class="invalid-feedback" role="alert">--}}
                    {{--<strong>{{ $message }}</strong>--}}
                {{--</span>--}}
        {{--@enderror--}}

        {{--<p class='input-title login-title'>Пароль</p>--}}
        {{--<input id="password-login" required type="password" name="password" class="modal-input">--}}
        {{--@error('password')--}}
        {{--<span class="invalid-feedback" role="alert">--}}
                    {{--<strong>{{ $message }}</strong>--}}
                {{--</span>--}}
        {{--@enderror--}}

        {{--<div class="modal-bottom">--}}
            {{--<button type="submit" class="confirm-button btn">Войти</button>--}}
            {{--<p class="modal-or">или</p>--}}
            {{--<a href="#" class="register">Регистрация</a>--}}
        {{--</div>--}}
    {{--</form>--}}
{{--</div>--}}