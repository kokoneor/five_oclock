@extends('layouts.main')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
    <script src="/js/catalog.js"></script>
    <div class="catalog">
        <div class="container">
            <h1>Каталог</h1>
            <div class="breadcrumbs">
                <a href="/" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Каталог</p>
            </div>
            <div class="sort">
                <span>Сортировать</span>
                <div class="sort-select">
                    <div class="sort-dropdown">
                        <span class="sort-select_value">Низкая цена</span>
                        <i class="icon-right"></i>
                    </div>
                    <div class="sort-dropdown_list">
                        <div data-cost="Высокая цена" class="select-catalog"
                            onclick="composeSortFilter({type: 'sort', range: 'DESC'})">
                            Высокая цена
                        </div>
                        <div data-cost="Низкая цена" class="select-catalog"
                             onclick="composeSortFilter({type: 'sort', range: 'ASC'})">
                            Низкая цена
                        </div>
                    </div>
                </div>
            </div>
            <div class="catalog-content grid grid-3">
                <div class="filter">
                    <div class="filter-top">
                        <h2>Фильтр</h2>
                        <div class="btn filter-btn" id="clear-filter-btn" onclick="clearFilter()">Очистить</div>
                    </div>
                    <div class="section-filter">
                        <h3>Категории</h3>
                        @foreach($catalog as $category)
                            <div class="filter-categories">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="category-{{ $category->id }}" onclick="composeFilters({type: 'category', id: '{{ $category->id }}'}, 'category-{{ $category->id }}')">
                                    <label for="category-{{ $category->id }}">{{ $category->name  }}</label>
                                </div>
                                {{--<span></span>--}}
                            </div>
                        @endforeach
                    </div>
                    <div class="section-filter">
                        <h3>По цене</h3>
                        @foreach($filter_prices as $price)
                            <div class="filter-categories">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="filter-price-{{ $price->id }}"
                                           onclick="composeFilters({type: 'price', range: '{{ $price->from }}-{{ $price->to }}'}, 'filter-price-{{ $price->id }}')">
                                    <label for="filter-price-{{ $price->id }}">
                                        {{ $price->from }}-{{ $price->to }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="section-filter">
                        <h3>По размеру</h3>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <input type="checkbox" id="cb14">
                                <label for="cb14">1 Кг</label>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <input type="checkbox" id="cb15">
                                <label for="cb15">1 Кг</label>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <input type="checkbox" id="cb16">
                                <label for="cb16">2 Кг</label>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <input type="checkbox" id="cb17">
                                <label for="cb17">2 Кг</label>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="cb18">
                                    <label for="cb18">3 Кг</label>
                                </div>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="cb19">
                                    <label for="cb19">5 Кг</label>
                                </div>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="cb20">
                                    <label for="cb20">10 Кг</label>
                                </div>
                            </div>
                        </div>
                        <div class="filter-categories">
                            <div class="filter-checkbox">
                                <div class="filter-checkbox">
                                    <input type="checkbox" id="cb21">
                                    <label for="cb21">10 Кг</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="section-filter">--}}
                        {{--<h3>Теги</h3>--}}
                        {{--<div class="tags">--}}
                            {{--<div class="tag">Вкусно</div>--}}
                            {{--<div class="tag">Выпечка</div>--}}
                            {{--<div class="tag">Торт</div>--}}
                            {{--<div class="tag">Coffee</div>--}}
                            {{--<div class="tag">Chikoo</div>--}}
                            {{--<div class="tag">Лучшее</div>--}}
                            {{--<div class="tag">Выпечка</div>--}}
                            {{--<div class="tag">Вкусно</div>--}}
                            {{--<div class="tag">Десерт</div>--}}
                            {{--<div class="tag">Торт</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="filter-banner">
                        <img src="/img/banner-catalog.jpg" alt="Баннер">
                    </div>
                </div>
                <div class="filter-items_grid grid grid-3" id="productsFilter">


                </div>
            </div>
            {{--<div class="pagination pagination-catalog">--}}
                {{--<div class="pagination-count active">--}}
                    {{--1--}}
                {{--</div>--}}
                {{--<div class="pagination-count">--}}
                    {{--2--}}
                {{--</div>--}}
                {{--<div class="pagination-count">--}}
                    {{--3--}}
                {{--</div>--}}
                {{--<div class="pagination-count disabled">--}}
                    {{--...--}}
                {{--</div>--}}
                {{--<div class="pagination-count circle">--}}
                    {{--<i class="icon-arrow"></i>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection
