@extends('layouts.main')
@section('content')
    <div class="ours-page">
        <div class="ours-page_banner">
            <div class="container">
                <h1>О нас</h1>
                <div class="breadcrumbs ours-breadcrumbs">
                    <a href="#" class="breadcrumb-item">Главная</a>
                    <span>/</span>
                    <p class="page-here">О нас</p>
                </div>
                <div class="ours-text">
                    <h2>{!! $model->title !!}</h2>
                    <p>{{ $model->subtitle }}</p>
                </div>
            </div>
            <div class="ours-bottom">
                <div class="ours-bottom_content">
                    <div class="container">
                        <p> <b>Кондитерская Five O'Clock</b> - это небольшая сеть уютных и стильных кафе-кондитерских
                            европейского
                            стиля. Первая расположена на улице Кунаева, через дорогу от Торгового дома "Сымбат"</p>
                        <button class="btn btn-yellow ours-btn">В КАТАЛОГ</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="ours-styles">
            <div class="container">
                <div class="ours-styles_text">
                    {!! $model->description !!}
                </div>
                <div class="ours-slider">
                    <div class="swiper-cond">
                        <div class="swiper-wrapper">
                            <?php $gallery = json_decode($model->gallery); ?>
                            @foreach($gallery as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/' . $image) }}" alt="">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-navigation">
                            <div class="slider-left"><i class="icon-left"></i></div>
                            <span>/</span>
                            <div class="slider-right"><i class="icon-right"></i></div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="ours-descriptions">
                    {!! $model->content !!}
                </div>
                <div class="ours-images_grid grid">
                    <?php $images = json_decode($model->image); ?>
                    @foreach($images as $image)
                        @if($loop->first)
                            <div>
                                <img src="{{ asset('storage/' . $image) }}" alt="">
                                <a href="/catalog" class="btn btn-white">В КАТАЛОГ</a>
                            </div>
                        @elseif($loop->index == 1)
                            <div>
                                <img src="{{ asset('storage/' . $image) }}" alt="">
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div class="ours_card">
        <div class="container">
            @foreach($bonus_info as $info)
                @if($info->status == 1)
                    <div class="ours-card_top">
                        <h2>{!! $info->title !!}</h2>
                        {!! $info->content !!}
                    </div>
                    <a href="#" class="btn ours-card_btn">{{ $info->info }}</a>
                    <div class="graph">
                        <img src="img/graph.svg" alt="">
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <section class="bonus-card">
        <div class="container">
            @foreach($bonus_info as $info)
                @if($info->status == 2)
                    <div class="bonus-card_grid grid">
                        <div>
                            <img src="{{ asset('storage/' . $info->image) }}" alt="Бонусная карта">
                            <p>{!! $info->info !!}</p>
                        </div>
                        <div>
                            <h2>{!! $info->title !!}</h2>
                            {!! $info->content !!}
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </section>
@endsection