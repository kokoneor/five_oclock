@extends('layouts.main')
@section('content')
    <div class="promo-page">
        <div class="container">
            <h1>Акции</h1>
            <div class="breadcrumbs">
                <a href="/" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Акции</p>
            </div>
            <section class="promo-page_timer">
                <h2>До начала Акции</h2>
                <div class="timer">
                    <div class="days">
                        <p class="days-count">21</p>
                        <!-- <span class="colon">:</span> -->
                        <span>Дней</span>
                    </div>
                    <div class="minutes">
                        <p class="minutes-count">24 </p>
                        <span>минут</span>
                    </div>
                    <div class="hours">
                        <p class="hours-count">59</p>
                        <span>часов</span>
                    </div>
                    <div class="seconds">
                        <p class="seconds-count">45</p>
                        <span>секунд</span>
                    </div>
                </div>
            </section>
            <section class="awards">
                <form id="takePartBtn" name="takePartBtn">
                    <div class="awards-main_bg">
                        <img src="{{ asset('storage/' . $model->image) }}" alt="Картинка">
                    </div>
                    <div class="awards-bottom">
                        <div class="awards-users">
                            {{--<div class="awards-user">--}}
                                {{--<img src="img/user-1.jpg" alt="">--}}
                                {{--<p>Котов Михаил</p>--}}
                            {{--</div>--}}

                        </div>
                        <div class="awards-info">
                            <div class="awards-top">
                                <p>{{ $model->title }}</p>
                            </div>
                            <div class="awards-content">
                                <h3>{{ $model->name }}</h3>
                                <p>{{ $model->description }}</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="promo_id" value="{{ $model->id }}">
                    <input type="hidden" name="user_id" id="promoUserId" value="{{ $user_id }}">

                    @if(!$promoUser)
                        <button type='submit' class="btn awards-btn" >Участвовать</button>
                    @endif
                </form>
            </section>

        </div>
    </div>
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
    <script src="/js/request.js"></script>
@endsection
