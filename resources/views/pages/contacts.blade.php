@extends('layouts.main')
@section('content')
    <div class="contacts-page">
        <div class="container">
            <h1>Контакты</h1>
            <div class="breadcrumbs contacts-breadcrumbs">
                <a href="#" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">Контакты</p>
            </div>
        </div>
        <div class="yandex-maps">
            {!! $model->map !!}
        </div>
        <div class="contacts-information">
            <div class="container">
                <div class="contacts_grid grid grid-3">
                    <div>
                        <h3>Телефон</h3>
                        <p><i class="icon-telephone"></i><span>{{ $model->phone }}</span></p>
                    </div>
                    <div>
                        <h3>Адрес</h3>
                        <p><i class="icon-address"></i><span>{{ $model->address }}</span></p>
                    </div>
                    <div>
                        <h3>Социальные сети</h3>
                        <div class="social-blocks">
                            <i class="icon-repost"></i>
                            <div>
                                <a href="#"><img src="img/icons/twitter.svg" alt="Twitter" title="Twitter"></a>
                                <a href="#"><img src="img/icons/facebook-icon.svg" alt="Facebook" title="Facebook"></a>
                                <a href="#"><img src="img/icons/vk-icon.svg" alt="Вконтакте" title="Вконтакте"></a>
                                <a href="#"><img src="img/icons/instagram-icon.svg" alt="Instagram"
                                        title="Instagram"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contacts-subscribe">
                    <h2>Подписаться на рассылку</h2>
                    <div class="contacts-subscribe_content">
                        <p>"По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который
                            вызывает у
                            некторых людей"</p>
                        <form>
                            <input type="text" name="name" id="name-subscriber" placeholder="Имя">
                            <input type="email" name="email" id="email-subscriber" placeholder="Электронная почта">
                            <button type="submit" class="btn btn-subscriber">Подписаться</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection