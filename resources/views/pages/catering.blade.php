@extends('layouts.main')
@section('content')
    <div class="smart-page">
        <div class="container">
            <h1>{{ $catering->title }}</h1>
            <div class="breadcrumbs smart-breadcrumbs">
                <a href="/" class="breadcrumb-item">Главная</a>
                <span>/</span>
                <p class="page-here">{{ $catering->title }}</p>
            </div>
            <div class="grid smart-info_grid">
                <div>
                    {!! $catering->description !!}
                </div>
                <div>
                    <img src="img/question.svg" alt="">
                </div>
            </div>
            <div class="catering-lists grid grid-3">
                {!! $catering->list !!}
            </div>
            <div class="catering-sliders">
                <div class="catering-prev"><i class="icon-left"></i></div>
                <div class="catering-next"><i class="icon-right"></i></div>
                <div class="swiper catering-swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="catering-slider">
                                <h2>Присоединяйтесь к клубу Club Sandwich каждое утро!</h2>
                                <a href="#" class="btn btn-yellow btn-slider">В каталог</a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catering-slider">
                                <h2>Присоединяйтесь к клубу Club Sandwich каждое утро!</h2>
                                <a href="#" class="btn btn-yellow btn-slider">В каталог</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="catering-info">
            <div class="container">
                <div class="catering-info_grid grid grid-3">
                    {!! $catering->info !!}
                </div>
            </div>
        </div>
        <div class="quickly">
            <div class="container">
                <h2>Быстро, вкусно и посуду мыть не нужно</h2>
                <div class="quckly_grid grid grid-3">
                    <div class="quckly_card">
                        <h3>предзаказ</h3>
                        <p>На данный момент работаем в режиме предзаказа: <b>делай заказ сегодня и получай завтра</b>
                        </p>
                    </div>
                    <div class="quckly_card">
                        <h3>Экономия</h3>
                        <p>Вам не нужно готовить, выдумывать меню, мыть посуду</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Гибкость</h3>
                        <p>В нашем арсенале есть стандартное меню для всех видов мероприятий: <b>фуршета, корпоратива,
                                бизнес-обеда, коктейля, кофе-брейка </b>. Но мы привыкли быть гибкими и оформим ваш
                            заказ по
                            индивидуальному запросу: с учетом комфортной стоимости и числа гостей</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Качество</h3>
                        <p>Мы готовим все блюда из натуральных продуктов. Готовая еда уезжает к вам в офис, на
                            мероприятие или домой из-под ножа. <b>Smart Catering Five O'Clock</b> - это
                            свежеприготовленные
                            закуски и выпечка</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Профессионализм</h3>
                        <p> <b>Мы готовим всю еду самостоятельно </b> - от закусок до самых сложных кремов на вашем
                            торте. Сами
                            взбиваем соус Голландез, печем хлеб, жарим мясо и не только</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Smart Catering 5 O'Clock</h3>
                        <p>Это гармония качества, эстетики и приемлемой цены</p>
                    </div>
                    <div class="quckly_card">
                        <h3>Цена</h3>
                        <p> <b>Smart Catering Five O'Clock</b> - ваш обед не отличить от ресторанного. Зато его
                            стоимость будет
                            более доступной</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ours-clients">
            <div class="container">
                <div class="ours-clients_content">
                    {!! $catering->client_content !!}
                    <a href="{{ $catering->client_link }}" class="btn btn-yellow btn-slider">Наши клиенты</a>
                </div>
            </div>
        </div>
    </div>

    <section class="catering">
        <div class="container">
            @foreach($events as $event)
                @if($event->is_main == 1 && $loop->first)
                    <h2 class="title">Кейтеринг мероприятия</h2>
                    <div class="catering-grid grid">
                        <div class="photo_date">
                            <div class="main-event">
                                <img class="catering-mainBg" src="{{ asset('storage/' . $event->image) }}" alt="текст">
                                <div class="additional-event">
                                    <?php $images = json_decode($event->images); ?>
                                    @if(!empty($images))
                                        @foreach($images as $image)
                                            <img class="additional-img" src="{{ asset('storage/' . $image) }}" alt="">
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="event-date">
                                <div class="event-start">
                                    <p class="date-subtitle">Дата начала:</p>
                                    <div class="event-numbber">
                                        <h3>{{ $event->date_start }} <span>года</span></h3>
                                    </div>
                                </div>
                                <div class="event-end">
                                    <p class="date-subtitle">Дата завершения:</p>
                                    <div class="event-numbber">
                                        <h3>{{ $event->date_end }} <span>года</span></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="event-content">
                            <h3 class="event-content-title">{{ $event->title }}</h3>
                            <p class="event-content-subtitle">{{ $event->name }}</p>
                            <p class="event-content-descr">{!! $event->description !!}</p>
                            <a href="{{ '/event/' . $event->id }}" class="event-content-more link">
                                ПОДРОБНЕЕ <i class="icon-right"></i>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="articles_grid grid grid-3">
                @foreach($events as $event)
                    @if($event->is_main == 0)
                        <div class="article">
                            <div class="article-img_info">
                                <img src="{{ asset('storage/' . $event->image) }}" alt="Название статьи">
                                <div class="article-date">с {{ $event->date_start }} до {{ $event->date_end }}</div>
                            </div>
                            <h3 class="article-name">{{ $event->name }}</h3>
                            <p class="article-descr">{{ $event->description }}</p>
                            <a href="{{ '/event/' . $event->id }}" class="link article-link">ПОДРОБНЕЕ <i class="icon-right"></i></a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection