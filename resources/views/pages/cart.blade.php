@extends('layouts.main')
@section('content')
<div class="cart">
    <div class="container">
        <h1>Корзина</h1>
        <div class="breadcrumbs">
            <a href="/" class="breadcrumb-item">Главная</a>
            <span>/</span>
            <p class="page-here">Корзина</p>
        </div>
        <p class="cart-counts"><span class="cart-counter-by-hikaro"></span>товара в Вашей корзине</p>
        <div id="cartTotal">
            <div class="cart-content">
                <div class="cart-top">
                    <div>
                        <p>Товар</p>
                    </div>
                    <div>
                        <p>Количество</p>
                        <p>Цена</p>
                    </div>
                </div>
                <div class="cart-items" id="cart-items">

                </div>
            </div>
            <div class="cart-total">
                <div class="cart-total_main">
                    <p>Итого:</p>
                    <p id="total-cart">0</p>
                </div>
                @auth
                    <?php $userBonus = $user->userBonus; ?>
                    @if($userBonus)
                        <input type="hidden" name="cashback" id="userBonusCashBack" class="user-bonus-cash-back"
                               value="{{ $userBonus->bonus }}">
                        <input type="hidden" name="user-bonus-balance" class="user-bonus-balance" id="userBonusBalance"
                               value="{{ $userBonus->balance }}">
                    @else
                        <input type="hidden" name="cashback" id="userBonusCashBack" class="user-bonus-cash-back" value="10">
                        <input type="hidden" name="user-bonus-balance" class="user-bonus-balance" id="userBonusBalance"  value="0">
                    @endif
                    <div>
                        <p>К начислению Cash-Back</p>
                        <p class="order-cash-back" id="orderCashBack">0</p>
                    </div>
                    <div class="cart-total_right">
                        <div class="cashback">
                            <p>Потратить Cash-back</p>
                            <div class="cashback_bottom">
                                <p>Доступно: <span id="in-access-user-balance">0</span></p>
                                <input onchange="balanceUser()" class="toggle" type="checkbox" id="checkboxBalanceCart" value="0"/>
                                <label class="toggle-label" for="checkboxBalanceCart">Toggle</label>
                            </div>
                        </div>
                        <p class="order-user-balance" id="access-user-balance">0</p>
                    </div>
                @endauth
                <div class="cart-total_delivery" id="cartTotalDelivery">
                    <p>ДОСТАВКА</p>
                    <p id="delivery-price">1000</p>
                    <input type="hidden" name="delivery" id="deliveryPriceCart" value="1000">
                </div>
                <div class="cart-total_bottom">
                    <p>Итого к оплате:</p>
                    <p id="total-price">0</p>
                </div>
                <div class="cart-total_buttons">
                    @guest
                        <button class="btn cart-total_btn">Оформить заказ без регистрации</button>
                        <button class="btn cart-total_btn btn-outline sign-up">Зайти в аккаунт и оформить заказ</button>
                    @endguest
                </div>
            </div>
            <div class="cart-contacts">
            <p>ВАШИ КОНТАКТЫ:</p>
            <form class="cart-contacts-form" name="orderCartForm" id="orderCartForm">
                <div class="cart-contacts_inputs">
                    @auth
                        <input type="text" required class="contacts-input" name="username"
                               placeholder="Имя" value="{{ $user->name }}">

                        <input type="text" class="contacts-input"  name="surname"
                               placeholder="Фамилия" value="{{ $user->surname }}">

                        <input type="tel" required class="contacts-input" name="phone" id="order_phone"
                               placeholder="Телефон" value="{{ $user->phone }}">
                        <input type="email" required class="contacts-input"  name="email"
                               placeholder="Электронный адрес" value="{{ $user->email }}">
                    @else
                        <input type="text" required class="contacts-input" name="username"
                               placeholder="Имя" value="">

                        <input type="text" class="contacts-input" name="surname"
                               placeholder="Фамилия" value="">

                        <input type="tel" required class="contacts-input" name="phone" id="order_phone"
                               placeholder="Телефон" value="">
                        <input type="email" required class="contacts-input"  name="email"
                               placeholder="Электронный адрес" value="">
                    @endauth

                </div>
                <div class="toggles">
                    <div>
                        <input onchange="setDeliveryCart()" type="radio" name="rb" id="deliveryCheckboxCart" checked> <label
                            for="deliveryCheckboxCart">Доставка</label>
                    </div>
                    <div>
                        <input onchange="setPickupCart()" type="radio" name="rb" id="pickupCheckboxCart"> <label
                            for="pickupCheckboxCart">Самовывоз</label>
                    </div>
                </div>
                <div class="delivery_form">
                    <p>АДРЕС ДОСТАВКИ:</p>
                    <div class="delivery-forms_inputs">
                        <div class="delivery-forms_inputs">
                            <input type='text' class="contacts-input" name="address" placeholder="Улица">
                            <input type='text' class="contacts-input" name="builder" placeholder="Дом">
                            <input type='text' class="contacts-input" name="apartment"  placeholder="Квартира">
                            <input type='text' class="contacts-input" name="message"
                                placeholder="Дополнительная информация">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="full_price" id="order_full_price">
                <input type="hidden" name="user_balance" id="order_user_balance">
                <input type="hidden" name="products" id="order_products">
                <input type="hidden" name="order_type" id="order_type_cart" value="3">
                <input type="hidden" name="delivery_type" id="delivery_type_cart" value="1">
                @auth
                    <input type="hidden" name="user_id" id="userId" value="{{ $user->id }}">
                @else
                    <input type="hidden" name="user_id" id="userId">
                @endauth

                <button type='submit' class="btn-contacts btn">Заказать</button>
            </form>
        </div>
        </div>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="/js/request.js"></script>
    <script>
        $("#order_phone").mask("+7(999)999-99-99");
        getProductsFromAPI()
    </script>
@endsection
