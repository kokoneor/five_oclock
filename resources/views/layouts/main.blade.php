<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Five oClock</title>
    <!-- Bootstrap CSS (Cloudflare CDN) -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" integrity="sha512-P5MgMn1jBN01asBgU0z60Qk4QxiXo86+wlFahKrsQf37c9cro517WzVSPPV1tDKzhku2iJ2FVgL67wG03SGnNA==" crossorigin="anonymous">--}}

    <link rel="stylesheet" href="{{ mix('/css/style.css') }}">
    {{--<!-- jQuery (Cloudflare CDN) -->--}}
    {{--<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>--}}

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>--}}

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="/js/cart.js"></script>

</head>

<style>
    .card-btn-cart {
        font-size: 12px;
        padding: 20px 30px;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .card-btn-cart i {
        margin-right: 10px;
        font-size: 17px;
    }
</style>

<body>

    <div class="cooking-bg">
        <div id="cooking">
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div class="bubble"></div>
            <div id="area">
                <div id="sides">
                    <div id="pan"></div>
                    <div id="handle"></div>
                </div>
                <div id="pancake">
                    <div id="pastry"></div>
                </div>
            </div>
        </div>
    </div>
    <header class="header">
        <nav class="nav">
            <div class="burger">
                <div class="burger-bg">
                    <div class="additional-burger">
                        <div class="top-bun"></div>
                        <div class="meat"></div>
                        <div class="bottom-bun"></div>
                        <div class="popup-menu">
                            <div class="popup-grid grid grid-3">
                                @foreach($categories as $category)
                                    <div class="popup-block">
                                        @foreach($category as $item)
                                            {{--@if(is_null($category->children))--}}
                                                <ul>
                                                    <li><a href="/catalog">{{ $item->name }}</a></li>
                                                </ul>
                                            {{--@else--}}
                                                {{--<details>--}}
                                                    {{--<summary>{{ $category->name }}</summary>--}}
                                                    {{--<ul>--}}
                                                        {{--@foreach($category->children as $child)--}}
                                                            {{--<li><a href="#">{{ $child->name }}</a></li>--}}
                                                        {{--@endforeach--}}
                                                    {{--</ul>--}}
                                                {{--</details>--}}
                                            {{--@endif--}}
                                        @endforeach
                                    </div>

                                @endforeach

                                <div class="popup-block">
                                    <img src="/img/banner.jpg" alt="Баннер">
                                    <h3>Coffee Five O’clock</h3>
                                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="/" class="logo">
                <img src="/img/logo.svg" alt="Five oClock">
            </a>
            <ul class="nav__menu">
                @foreach(menu('site', '_json') as $menuItem)
                    <li class="nav__menu-item">
                        <a href="{{ $menuItem->url }}" class="nav__menu-link">
                            {{ $menuItem->title }}
                        </a>
                    </li>
                @endforeach


                {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Личный кабинет</a></li>--}}

                <li class="nav__menu-item mobile-list"><a href="#" class="nav__menu-link url-flex">Доп меню <i
                                class="icon-right"></i></a></li>
                <ul class="submenu">
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">СМОТРЕТЬ ВСЕ</a></li>
                    @foreach($categories as $category)
                        @foreach($category as $item)
                            <li class="nav__menu-item"><a href="#" class="nav__menu-link">
                                    {{ $item->name }}
                                </a></li>
                        @endforeach
                    @endforeach
                    {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Coffee</a></li>--}}
                    {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Кондитерка</a></li>--}}
                    {{--<li class="nav__menu-item mobile-list"><a href="#" class="nav__menu-link url-flex">Выпечка <i--}}
                                    {{--class="icon-right"></i></a></li>--}}
                    {{--<ul class="submenu">--}}
                        {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Cдоба</a></li>--}}
                        {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Пироженные</a></li>--}}
                        {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Торты</a></li>--}}
                    {{--</ul>--}}
                    {{--<li class="nav__menu-item"><a href="#" class="nav__menu-link">Контакты</a></li>--}}
                    <li class="nav__menu-item"><a href="#" class="nav__menu-link">Личный кабинет</a></li>
                </ul>
            </ul>
            <button class="btn btn-nav">Индивидуальный заказ</button>
            <button class="btn-nav mobile-hide"><i class="icon-telephone"></i></button>

            <div class="header-user">
                <a href="/cart" class="cart-user"><i class="icon-cart"></i>
                    <span class="cart-counter cart-counter-by-hikaro"></span>
                </a>
                @auth <a href="/profile" class="user"><img src="/img/user.jpg" alt=""></a>@endauth
            </div>

            @guest
                <div class="sign-up">
                    <a href="#">
                    {{--<a href="#" data-toggle="modal" data-target="#authorizationModal">--}}
                        <i class="icon-signup"></i>
                    </a>
                </div>
            @endguest
        </nav>
    </header>

    @yield('content')

    <section class="company">
        <div class="container">
            <div class="company-grid grid grid-3">
                <div class="company-block">
                    <h2 class="company-title">{{ $about_footer->title }} <span>Five O'Clock</span></h2>
                    <a href="/about" class="btn btn-company">ПОДРОБНЕЕ</a>
                </div>
                {!! $about_footer->content !!}
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="footer-grid grid grid-4">
                <div class="footer-block">
                    <img class="logo-white" src="/img/logo-white.svg" alt="Лого">
                </div>
                <div class="footer-block">
                    <h3 class="footer-title">График работы</h3>
                    <div class="footer-text">
                        {!! $contacts->job !!}
                    </div>
                </div>
                <div class="footer-block">
                    <h3 class="footer-title">Контакты</h3>
                    <div class="footer-text">
                        <p>{{ $contacts->address }}</p>
                        <a href="tel:{{ $contacts->phone }}">{{ $contacts->phone }}</a>
                        <a href="mailto:{{ $contacts->email }}">{{ $contacts->email }}</a>
                    </div>
                </div>
                <div class="footer-social">
                    @foreach($social_networks as $social_network)
                        <div class="{{ $social_network->description }}">
                            <i class="{{ $social_network->icon }}"></i>
                            <!-- <span>{{ $social_network->quantity }}</span> -->
                            <p>{{ $social_network->name }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
            <p class="footer-info">Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В
                то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem
                Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков,
                но и перешагнул в электронный дизайн.</p>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-content">
                    <p>Разработано "Shedry Studio"</p>
                    <p>© Все права защищены</p>
                    <a href="#" class="policy">Политика конфиденциальности</a>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ mix('js/main.js') }}"></script>
    <script>
        renderHeaderCounter()
    </script>

    @if(count( $errors ) > 0)
        <script>
            Swal.fire({
                icon: "error",
                title: "Ошибка",
                html: `
                    <ul>
                        @foreach($errors->all() as $error)

                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                `
            });
        </script>
        @php
            \Session::flush('errors');
        @endphp
    @endif

</body>

</html>
