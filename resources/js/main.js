import Swiper from "swiper/bundle";
import "swiper/css/bundle";
import Swal from "sweetalert2";
import IMask from "imask";

import axios from "axios";
// Слайдер
const swiper = new Swiper(".swiper-container", {
    speed: 1000,
    // Enable lazy loading
    lazy: true,
    updateOnWindowResize: true,
    pagination: {
        el: ".swiper-pagination",
        type: "progressbar",
        clickable: false,
        watchSlidesProgress: true,
    },
    autoplay: {
        delay: 5000,
    },
    effect: "fade",
    fadeEffect: {
        crossFade: true,
    },
    on: {
        slideChange: function () {
            const currentSlide = this.realIndex + 1;
            document.querySelector(".current-slide").innerHTML =
                "0" + currentSlide;
        },
        paginationRender: function () {
            var totalSlides = document.getElementsByClassName(
                "swiper-pagination-bullet"
            ).length;
            document.querySelector(".total-slides").innerHTML =
                "0" + this.slides.length;
        },
    },
});

// Модалки
const signup = document.querySelectorAll(".sign-up");
const btnOrder = document.querySelectorAll(".btn-nav");
btnOrder.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        formFill();
    });
});

signup.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        loginForm();
    });
});

// Форма входа
const loginForm = () => {
    Swal.fire({
        title: "Войти",
        customClass: {
            popup: "login-container",
            title: "modal-title",
            confirmButton: "confirm-button btn",
        },
        confirmButtonText: "Ок",
        showConfirmButton: false,

        html:
            '<form method="post" action="/login" id="loginFormModal" class="login-form">' +
            `<input type="hidden" value="${document
                .querySelector('meta[name="csrf-token"]')
                .getAttribute("content")}" name="_token" >` +
            '   <p class="input-title login-title">' +
            "       Электронная почта" +
            "   </p>" +
            '   <input id="email-login" required type="email" name="email" class="modal-input">' +
            "   <p class='input-title login-title'>" +
            "       Пароль" +
            "   </p>" +
            '   <input id="password-login" required type="password" name="password" class="modal-input">' +
            '   <div class="modal-bottom">' +
            '       <button type="submit" class="confirm-button swal2-confirm btn">' +
            "           Войти" +
            "       </button>" +
            '       <p class="modal-or">' +
            "           или" +
            "       </p>" +
            '       <a href="#" class="register">' +
            "           Регистрация" +
            "       </a>" +
            "   </div>" +
            "</form>",
    }).then((result) => {
        if (result.isConfirmed) {
            document.forms.loginFormModal.submit();
        } else if (result.isDenied) {
            return false;
        }
    });

    const loginBtn = document.querySelector(".confirm-button");
    loginBtn.addEventListener("click", (e) => {
        e.preventDefault();
        Swal.clickConfirm();
    });
    const register = document.querySelector(".register");

    register.addEventListener("click", (e) => {
        e.preventDefault();
        regForm();
    });
};

// Форма регистрации
const regForm = () => {
    Swal.fire({
        title: "Регистрация",
        customClass: {
            container: "...",
            popup: "login-container",
            title: "modal-title",
            confirmButton: "confirm-button btn",
        },
        confirmButtonText: "Ок",
        showConfirmButton: false,
        preConfirm: function () {
            if (document.forms.registrFormModal.checkValidity()) {
                document.forms.registrFormModal.submit();
            }
        },
        didRender: function () {
            const phone = document.getElementById("phone");
            const maskOptions = {
                mask: "+{7}(000)000-00-00",
                lazy: false,
            };
            const mask2 = IMask(phone, maskOptions);
        },
        html:
            '<form method="post" action="/register" class="login-form register-form" id="registrFormModal">' +
            `   <input type="hidden" value="${document
                .querySelector('meta[name="csrf-token"]')
                .getAttribute("content")}" name="_token" >` +
            '   <p class="input-title login-title">' +
            "      Имя" +
            "   </p>" +
            '   <input min="3" required id="name-reg" type="text" name="name" class="modal-input">' +
            '   <p class="input-title login-title">' +
            "      Электронная почта" +
            "   </p>" +
            '   <input min="6" required id="email-reg" type="email" name="email" class="modal-input">' +
            "   <p class='input-title login-title'>" +
            "      Пароль" +
            "   </p>" +
            '   <input min="6" required id="password-reg" type="password" name="password" class="modal-input">' +
            '   <p class="input-title login-title">' +
            "      Подтвердите пароль" +
            "   </p>" +
            '   <input required id="password-confirm" type="password" name="password_confirmation" class="modal-input">' +
            '   <p class="input-title login-title">' +
            "      Телефон" +
            "   </p>" +
            '   <input required id="phone" min="11" type="text" name="phone" class="modal-input">' +
            '   <div class="modal-bottom">' +
            '      <button type="submit" class="confirm-button swal2-confirm btn register">' +
            "           Регистрация" +
            "      </button>" +
            '      <p class="modal-or">или</p>' +
            '      <a href="#" class="sign-up modal-link">' +
            "          Войти" +
            "      </a>" +
            "   </div>" +
            "</form>",
    });
    const signup = document.querySelector(".modal-link");
    signup.addEventListener("click", (e) => {
        e.preventDefault();
        loginForm();
    });
};

const regSpecial = document.querySelectorAll(".ours-card_btn");

regSpecial.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        regForm();
    });
});


// Модалка заполнения формы
const formFillClick = document.querySelectorAll(".form-fill");

formFillClick.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
    });
});

const formFill = () => {
    const data = axios.get("api/order/special-product").then((response) => {
        const holidays = response.data.holidays;
        const products = response.data.products;
        const dropdownHoliday = document.querySelector("#holidayDropdown");
        const dropdownProducts = document.querySelector("#productDropdown");
        const addProducts = document.querySelector("#addProducts");
        const selectorCakes = document.querySelector(".cakes-selector");
        const selectorHoliday = document.querySelector(".holiday-selector");
        dropdownHoliday.addEventListener("click", (e) => {
            const target = e.target;
            holiday.innerHTML = target.getAttribute("data-value");
            holiday.setAttribute(
                "data-value",
                target.getAttribute("data-value"),
                holiday.setAttribute("data-id", target.getAttribute("data-id"))
            );
        });

        dropdownProducts.addEventListener("click", (e) => {
            const target = e.target;
            cakes.innerHTML = target.getAttribute("data-value");
            cakes.setAttribute("data-value", target.getAttribute("data-value"));
            cakes.setAttribute("data-id", target.getAttribute("data-id"));
        });

        addProducts.addEventListener("click", (e) => {
            const target = e.target;
            additionalVariant.innerHTML = target.getAttribute("data-value");
            additionalVariant.setAttribute(
                "data-value",
                target.getAttribute("data-value")
            );
        });
        holidays.map((holiday) => {
            dropdownHoliday.innerHTML += `<div data-value="${holiday.name}" data-id="${holiday.id}" class="dropdown-list_item holiday-select">${holiday.name}</div>`;
        });

        products.map((product) => {
            dropdownProducts.innerHTML += `<div data-value="${product.name}" data-id="${product.id}" class="dropdown-list_item cakes-select">${product.name}</div>`;
            addProducts.innerHTML += `<div data-value="${product.name}" class="dropdown-list_item additional-product">${product.name}</div>`;
        });
    });

    Swal.fire({
        title: "Заполните форму индивидуального заказа",
        customClass: {
            popup: "login-container",
            title: "modal-title",
            confirmButton: "confirm-button btn",
        },
        confirmButtonText: "Ок",
        showConfirmButton: false,
        didRender: function () {
            const tel = document.getElementById("tel");
            const maskOptions = {
                mask: "+{7}(000)000-00-00",
                lazy: false,
            };
            const mask = IMask(tel, maskOptions);
        },
        html:
            '<span class="modal-subtitle">Наш сотрудник свяжется с вами в течении 5 минут</span>' +
            '<form class="formFillForm">' +
            '<input id="name" type="text" name="name" placeholder="Ваше имя" class="modal-input">' +
            '<input id="tel" type="text" name="tel" placeholder="Ваш номер телефона" class="modal-input">' +
            '<input id="delivery" type="text" name="delivery" placeholder="Адрес доставки" class="modal-input">' +
            '<div class="select">' +
            '<div class="dropdown">' +
            '<span data-value="Выбор праздника" data-id="0" class="holiday holiday-selector">Выбор праздника</span>' +
            '<i class="icon-right"></i>' +
            "</div>" +
            '<div class="dropdown-list" id="holidayDropdown">' +
            // '<div data-value="Праздник 1" class="dropdown-list_item holiday-select">Праздник 1</div>' +
            // '<div data-value="Праздник 2" class="dropdown-list_item holiday-select">Праздник 2</div>' +
            // '<div data-value="Праздник 3" class="dropdown-list_item holiday-select">Праздник 3</div>' +
            "</div>" +
            "</div>" +
            '<div class="select">' +
            '<div class="dropdown">' +
            '<span data-value="Выбор Торта" data-id="0" class="cakes cakes-selector">Выбор Торта</span>' +
            '<i class="icon-right"></i>' +
            "</div>" +
            '<div class="dropdown-list" id="productDropdown">' +
            // '<div data-value="Торт 1" class="dropdown-list_item cakes-select">Торт 1</div>' +
            // '<div data-value="Торт 2" class="dropdown-list_item cakes-select">Торт 2</div>' +
            // '<div data-value="Торт 3" class="dropdown-list_item cakes-select">Торт 3</div>' +
            "</div>" +
            "</div>" +
            '<div class="select">' +
            '<div class="dropdown">' +
            '<span data-value="Выбор доп.товара" class="additional-variant">Выбор доп.товара</span>' +
            '<i class="icon-right"></i>' +
            "</div>" +
            '<div class="dropdown-list" id="addProducts">' +
            // '<div data-value="Торт 1" class="dropdown-list_item variant-select">Торт 1</div>' +
            // '<div data-value="Торт 2" class="dropdown-list_item variant-select">Торт 2</div>' +
            // '<div data-value="Торт 3" class="dropdown-list_item variant-select">Торт 3</div>' +
            "</div>" +
            "</div>" +
            '<button type="submit" class="confirm-button swal2-confirm btn btn-fillForm">Заказать</button>' +
            "</form>",
    }).then((result) => {
        if (result.isConfirmed) {
            let dataObject = {
                phone: document.getElementById("tel").value,
                address: document.getElementById("delivery").value,
                holiday_id: holiday.getAttribute("data-id"),
                product_id: cakes.getAttribute("data-id"),
                username: document.getElementById("name").value,
            };

            axios
                .post("/api/order/special", {
                    username: dataObject.username,
                    phone: dataObject.phone,
                    address: dataObject.address,
                    holiday_id: dataObject.holiday_id,
                    product_id: dataObject.product_id,
                    header: {
                        "Content-type": "application/json",
                    },
                })
                .then((response) => {
                    successForm();
                    const name = document.querySelector("#name").value.length;
                    const tel = document.querySelector("#tel").value.length;
                    const delivery =
                        document.querySelector("#delivery").value.length;
                    if (
                        isConfirm &&
                        name !== 0 &&
                        tel !== 0 &&
                        delivery !== 0
                    ) {
                        successForm();
                    } else {
                        if (tel < 11 || tel > 12) {
                            Swal.fire({
                                icon: "error",
                                title: "Ошибка!",
                                showConfirmButton: false,
                                text: "Введите корректный номер телефона.",
                                timer: 2500,
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Ошибка!",
                                showConfirmButton: false,
                                text: "Заполните все необходимые поля",
                                timer: 2500,
                            });
                        }
                    }
                })
                .catch((e) => {
                    let errors = [];
                    for (let [key, value] of Object.entries(
                        e.response.data.errors
                    )) {
                        errors.push(value);
                    }
                    console.log(e);
                    Swal.fire({
                        icon: "error",
                        title: "Ошибка!",
                        showConfirmButton: false,
                        text: errors.join("\n"),
                    });
                });
        } else if (result.isDenied) {
            return false;
        }
    });
    const dropdownList = document.querySelectorAll(".dropdown-list_item");
    const holiday = document.querySelector(".holiday");
    const holidaySelect = document.querySelectorAll(".holiday-select");
    const select = document.querySelectorAll(".select");
    const cakes = document.querySelector(".cakes");
    const cakeSelect = document.querySelectorAll(".cakes-select");
    const additionalVariant = document.querySelector(".additional-variant");
    const variantSelect = document.querySelectorAll(".variant-select");

    for (var i = 0; i < dropdownList.length; i++) {
        dropdownList[i].addEventListener("click", function () {
            // remove active class for all elements
            for (var i = 0; i < dropdownList.length; i++) {
                dropdownList[i].classList.remove("active");
            }
            // add active to clicked element
            this.classList.add("active");
        });
    }

    select.forEach((item) => {
        item.addEventListener("click", () => {
            item.classList.toggle("active");
        });
    });

    holidaySelect.forEach((item) => {
        item.addEventListener("click", (e) => {
            holiday.innerHTML = item.getAttribute("data-value");
            holiday.setAttribute("data-value", item.getAttribute("data-value"));
        });
    });

    cakeSelect.forEach((item) => {
        item.addEventListener("click", (e) => {
            cakes.innerHTML = item.getAttribute("data-value");
            cakes.setAttribute("data-value", item.getAttribute("data-value"));
        });
    });

    variantSelect.forEach((item) => {
        item.addEventListener("click", (e) => {
            additionalVariant.innerHTML = item.getAttribute("data-value");
            additionalVariant.setAttribute(
                "data-value",
                item.getAttribute("data-value")
            );
        });
    });
    const btnFillForm = document.querySelector(".btn-fillForm");
    btnFillForm.addEventListener("click", (e) => {
        e.preventDefault();
        Swal.clickConfirm();
    });
};

// Модалка после заполнения
const successForm = () => {
    Swal.fire({
        icon: "success",
        title: "Спасибо!",
        showConfirmButton: false,
        text: "Мы обязательно свяжемся с Вами в ближайшее время",
        timer: 2500,
    });
};

// Модалка быстрой покупки
const goOrder = (product_id) => {
    Swal.fire({
        title: "Быстрая покупка",
        customClass: {
            popup: "login-container",
            title: "modal-title",
            confirmButton: "confirm-button btn",
        },
        confirmButtonText: "Ок",
        showConfirmButton: false,
        // preConfirm: function () {
        //     if (document.forms.orderFastFormModal.checkValidity()) {
        //         document.forms.orderFastFormModal.submit();
        //     }
        // },
        didRender: function () {
            const tel = document.getElementById("tel-fast");
            const maskOptions = {
                mask: "+{7}(000)000-00-00",
                lazy: false,
            };
            const mask = IMask(tel, maskOptions);
        },
        html:
            '<span class="modal-subtitle">Наш сотрудник свяжется с вами в течении 5 минут</span>' +
            '<form class="formFillForm" id="orderFastFormModal">' +
            `    <input type="hidden" value="${document
                .querySelector('meta[name="csrf-token"]')
                .getAttribute("content")}" name="_token" >` +
            `    <input id="product_id" type="hidden" name="product_id" data-value="${product_id}" class="modal-input">` +
            '    <input id="name-fast" type="text" name="name-fast" placeholder="Ваше имя" class="modal-input" required>' +
            '    <input id="tel-fast" type="text" name="phone-fast" placeholder="Ваш номер телефона" class="modal-input" required>' +
            '    <input id="delivery-fast" type="text" name="delivery-fast" placeholder="Адрес доставки" class="modal-input" required>' +
            '    <div class="counts-items-modal">' +
            '        <p class="modal-text-counts">Количество товаров</p>' +
            '            <div class="count-control">' +
            '                <button class="btn btn-yellow btn-count-modal minus">-</button>' +
            '                    <span class="total-modal modal-value" data-value="1">1</span>' +
            '                    <input id="quantity-total" type="hidden" name="quantity" value="1" class="quantity-total" disabled>' +
            '                <button class="btn btn-count-modal plus">+</button>' +
            "            </div>" +
            "    </div>" +
            '    <button type="submit" class="confirm-button swal2-confirm btn btn-fillForm">Заказать</button>' +
            "</form>",
    }).then((result) => {
        if (result.isConfirmed) {
            let dataObject = {
                username: document.querySelector("#name-fast").value,
                phone: document.querySelector("#tel-fast").value,
                address: document.querySelector("#delivery-fast").value,
                product_id: product_id,
                quantity: document
                    .querySelector(".modal-value")
                    .getAttribute("data-value"),
            };
            axios
                .post("/api/order/fast", {
                    username: dataObject.username,
                    phone: dataObject.phone,
                    address: dataObject.address,
                    product_id: dataObject.product_id,
                    quantity: dataObject.quantity,
                    header: {
                        "Content-type": "application/json",
                    },
                })
                .then((response) => {
                    successForm();
                    console.log(response);
                })
                .catch((e) => {
                    let errors = [];
                    for (let [key, value] of Object.entries(
                        e.response.data.errors
                    )) {
                        errors.push(value);
                    }
                    console.log(e);
                    Swal.fire({
                        icon: "error",
                        title: "Ошибка!",
                        showConfirmButton: false,
                        text: errors.join("\n"),
                    });
                });
        } else if (result.isDenied) {
            return false;
        }
    });
    // console.log(product_id)
    document.querySelector(".btn-fillForm").addEventListener("click", (e) => {
        e.preventDefault();
        Swal.clickConfirm();
    });

    let count = 1;
    const plusClass = document.querySelector(".plus");
    const countClass = document.querySelector(".total-modal");
    const minusClass = document.querySelector(".minus");

    countClass.innerHTML = count;
    plusClass.addEventListener("click", (e) => {
        e.preventDefault();
        countClass.innerHTML = ++count;
        document.getElementById("quantity-total").value = count + 1;
        countClass.setAttribute("data-value", count);
    });
    minusClass.addEventListener("click", (e) => {
        e.preventDefault();
        if (count > 1) {
            countClass.innerHTML = --count;
            document.getElementById("quantity-total").value = count - 1;
        }
    });
};

const cardOrder = document.querySelectorAll(".card-btn");

cardOrder.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        let product_id = item.getAttribute("data-id");
        goOrder(product_id);
    });
});

// Мобильное меню
const mobileList = document.querySelectorAll(".mobile-list");

mobileList.forEach((item) => {
    item.addEventListener("click", () => {
        item.classList.toggle("active");
    });
});

// Бургер меню
const burger = document.querySelector(".burger");
const navMenu = document.querySelector(".nav__menu");

burger.addEventListener("click", () => {
    navMenu.classList.toggle("active");
});

// Табы

const tab = document.querySelectorAll(".tab"),
    tabs = document.querySelector(".tabs"),
    tabContent = document.querySelectorAll(".tab-content");
const hideServiceContent = () => {
    tabContent.forEach((item) => {
        item.style.display = "none";
    });
    tab.forEach((item) => {
        item.classList.remove("active");
    });
};
const showServiceContent = (i = 0) => {
    if (tab[i] != undefined) {
        tab[i].classList.add("active");
        tabContent[i].style.display = "block";
    }
};
hideServiceContent();
showServiceContent();

if (tabs != undefined) {
    tabs.addEventListener("click", (e) => {
        const target = e.target;
        if (target && target.classList.contains("tab")) {
            tab.forEach((item, i) => {
                if (target == item) {
                    hideServiceContent();
                    showServiceContent(i);
                }
            });
        }
    });
}

// Кейтеринг
const mainImg = document.querySelector(".catering-mainBg");
const additionalImg = document.querySelectorAll(".additional-img");

additionalImg.forEach((item) => {
    item.addEventListener("click", () => {
        mainImg.src = item.src;
    });
});

// Каталог раскрывающийся список
const sortDropdown = document.querySelector(".sort-dropdown");
const sortSelectValue = document.querySelector(".sort-select_value");
const selectCatalog = document.querySelectorAll(".select-catalog");

for (let i = 0; i < selectCatalog.length; i++) {
    selectCatalog[i].addEventListener("click", function () {
        for (let i = 0; i < selectCatalog.length; i++) {
            selectCatalog[i].classList.remove("active");
        }
        this.classList.add("active");
    });
}
if (sortDropdown !== null) {
    sortDropdown.addEventListener("click", (e) => {
        const target = e.target;
        sortDropdown.classList.toggle("active");
    });
}

selectCatalog.forEach((item) => {
    item.addEventListener("click", () => {
        sortSelectValue.innerHTML = item.getAttribute("data-cost");
    });
});
if (sortDropdown) {
    window.addEventListener("click", (e) => {
        const target = e.target;
        if (
            !target.closest(".sort-dropdown") ||
            target.closest(".select-catalog")
        ) {
            sortDropdown.classList.remove("active");
        }
    });
}

// Загрузка страницы
const loader = document.querySelector(".cooking-bg");

window.addEventListener("load", () => {
    loader.style.opacity = "0%";
    // loader.style.display = 'none';

    setTimeout(() => {
        loader.remove();
    }, 600);
});

// Счетчик товара
// let count = 0;
// const startCount = 0;
const plusClass = document.querySelectorAll(".plus");
const countClass = document.querySelectorAll(".total-modal");
const minusClass = document.querySelectorAll(".minus");

if (countClass !== null) {
    for (let i = 0; i <= countClass.length; i++) {
        let startNumber = 0;
        // countClass[i].innerHTML = +startNumber;
        if (plusClass[i]) {
            plusClass[i].addEventListener("click", (e) => {
                e.preventDefault();
                countClass[i].innerHTML = ++startNumber;
            });
        }
        if (minusClass[i]) {
            minusClass[i].addEventListener("click", (e) => {
                e.preventDefault();
                if (startNumber >= 1) {
                    countClass[i].innerHTML = --startNumber;
                }
            });
        }
    }
}

// Изменение картинки в карточке товара
const cardItemPhoto = document.querySelector(".mainPhoto_img");
const additionalPhoto = document.querySelectorAll(".additional-photo_img");

additionalPhoto.forEach((item) => {
    item.addEventListener("click", () => {
        cardItemPhoto.src = item.src;
    });
});

// Таймер
if (document.querySelector(".days-count") !== null) {
    const deadline = "2021-09-01";

    function getTimeRemaining(endtime) {
        const t = Date.parse(endtime) - Date.parse(new Date()),
            days = Math.floor(t / (1000 * 60 * 60 * 24)),
            seconds = Math.floor((t / 1000) % 60),
            minutes = Math.floor((t / 1000 / 60) % 60),
            hours = Math.floor((t / (1000 * 60 * 60)) % 24);

        return {
            total: t,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        };
    }

    function getZero(num) {
        if (num >= 0 && num < 10) {
            return "0" + num;
        } else {
            return num;
        }
    }
    function setClock(selector, endtime) {
        const timer = document.querySelector(selector),
            days = timer.querySelector(".days-count"),
            hours = timer.querySelector(".hours-count"),
            minutes = timer.querySelector(".minutes-count"),
            seconds = timer.querySelector(".seconds-count"),
            timeInterval = setInterval(updateClock, 1000);
        updateClock();

        function updateClock() {
            const t = getTimeRemaining(endtime);

            days.innerHTML = getZero(t.days);
            hours.innerHTML = getZero(t.hours);
            minutes.innerHTML = getZero(t.minutes);
            seconds.innerHTML = getZero(t.seconds);

            if (t.total <= 0) {
                clearInterval(timeInterval);
            }
        }
    }
    setClock(".timer", deadline);
}

// Слайдер кондитерской
const swiperCond = new Swiper(".swiper-cond", {
    speed: 3000,
    spaceBetween: 30,
    // loop: true,
    // Enable lazy loading
    lazy: true,
    slidesPerView: 2,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    autoplay: {
        // delay: 1000,
    },
    navigation: {
        nextEl: ".slider-right",
        prevEl: ".slider-left",
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        520: {
            slidesPerView: 2,
        },
    },
});

// Табы в личном кабинете
const cabinetTab = document.querySelectorAll(".cabinet-tab"),
    cabinetTabs = document.querySelector(".cabinet-tabs"),
    cabinetTabContent = document.querySelectorAll(".cabinet-content");
const hideCabinetContent = () => {
    cabinetTabContent.forEach((item) => {
        item.style.display = "none";
    });
    cabinetTab.forEach((item) => {
        item.classList.remove("active");
    });
};
const showCabinetContent = (i = 0) => {
    if (cabinetTab[i] != undefined) {
        cabinetTab[i].classList.add("active");
        cabinetTabContent[i].style.display = "block";
    }
};
hideCabinetContent();
showCabinetContent();

if (cabinetTabs != undefined) {
    cabinetTabs.addEventListener("click", (e) => {
        const target = e.target;
        if (target && target.classList.contains("cabinet-tab")) {
            cabinetTab.forEach((item, i) => {
                if (target == item) {
                    hideCabinetContent();
                    showCabinetContent(i);
                }
            });
        }
    });
}

// Скрытие доставки по чекбоксу
function showDelivery() {
    document.querySelector(".delivery_form").style.display = "block";
}

function hideDelivery() {
    document.querySelector(".delivery_form").style.display = "none";
}

// Вариант продукта

const VariationList = document.querySelectorAll(".dropdown-list_variation");
const variation = document.querySelector(".variation");
const selectVariation = document.querySelectorAll(".select-variation");

for (var i = 0; i < VariationList.length; i++) {
    VariationList[i].addEventListener("click", function () {
        for (var i = 0; i < VariationList.length; i++) {
            VariationList[i].classList.remove("active");
        }
        this.classList.add("active");
    });
}

selectVariation.forEach((item) => {
    item.addEventListener("click", () => {
        item.classList.toggle("active");
    });
});

VariationList.forEach((item) => {
    item.addEventListener("click", (e) => {
        variation.innerHTML = item.getAttribute("data-variation");
        variation.setAttribute(
            "data-variation",
            item.getAttribute("data-variation")
        );
    });
});

// Слайдер на странице Smart

const smartSwiper = new Swiper(".catering-swiper", {
    // Optional parameters
    direction: "horizontal",
    loop: true,

    // Navigation arrows
    navigation: {
        nextEl: ".catering-next",
        prevEl: ".catering-prev",
    },
});

function saveToken(token) {
    localStorage.setItem("token", JSON.stringify(token));
}

// Модалка просмотра заказа

const fastOrder = (product_id) => {
    Swal.fire({
        title: "Заполните форму",
        customClass: {
            popup: "login-container",
            title: "modal-title",
            confirmButton: "confirm-button btn",
        },
        confirmButtonText: "Ок",
        showConfirmButton: false,
        html:
            '<span class="modal-subtitle">Наш сотрудник свяжется с вами в течении 5 минут</span>' +
            '<form class="formFillForm">' +
            '<input id="name-fastOrder" type="text" name="name-fastOrder" placeholder="Ваше имя" class="modal-input">' +
            '<input id="tel-fastOrder" type="number" name="tel-fastOrder" placeholder="Ваш номер телефона" class="modal-input">' +
            '<input id="delivery-fastOrder" type="text" name="delivery-fastOrder" placeholder="Адрес доставки" class="modal-input">' +
            '<input id="comment-fastOrder" type="text" name="comment-fastOrder" placeholder="Комментарий" class="modal-input">' +
            `<input id="product_id" type="hidden" name="comment-fastOrder">` +
            `<input id="calendar-fastOrder" max='${new Date().getFullYear()}-0${
                new Date().getMonth() + 2
            }-${new Date().getDate()}' min='${new Date().getFullYear()}-0${
                new Date().getMonth() + 1
            }-${new Date().getDate()}' value='${new Date().getFullYear()}-0${
                new Date().getMonth() + 1
            }-${new Date().getDate()}' type="date" name="calendar-fastOrder" placeholder="Комментарий" class="modal-input">` +
            '<button type="submit" class="confirm-button swal2-confirm btn btn-fillForm btn-b">Заказать</button>' +
            "</form>",
    }).then((result) => {
        if (result.isConfirmed) {
            let dataObject = {
                username: document.querySelector("#name-fastOrder").value,
                phone: document.querySelector("#tel-fastOrder").value,
                address: document.querySelector("#delivery-fastOrder").value,
                message: document.querySelector("#comment-fastOrder").value,
                order_date: document.querySelector("#calendar-fastOrder").value,
                product_id: product_id,
                quantity: 1,
            };
            axios
                .post("/api/order/to-order", {
                    username: dataObject.username,
                    phone: dataObject.phone,
                    address: dataObject.address,
                    message: dataObject.message,
                    order_date: dataObject.order_date,
                    product_id: dataObject.product_id,
                    quantity: dataObject.quantity,
                })
                .then((response) => {
                    Swal.fire({
                        icon: "success",
                        title: "Успешно",
                        showConfirmButton: false,
                        text: "Заказ принят в обработку!",
                    });
                })
                .catch((e) => {
                    let errors = [];
                    for (let [key, value] of Object.entries(
                        e.response.data.errors
                    )) {
                        errors.push(value);
                    }
                    console.log(e);
                    Swal.fire({
                        icon: "error",
                        title: "Ошибка!",
                        showConfirmButton: false,
                        text: errors.join("\n"),
                    });
                });
        } else if (result.isDenied) {
            return false;
        }
    });

    const btn = document.querySelector(".btn-b");

    btn.addEventListener("click", (e) => {
        e.preventDefault();
        Swal.clickConfirm();
    });
};
const btnBuy = document.querySelector(".btn-order");
if (btnBuy !== null) {
    btnBuy.addEventListener("click", () => {
        let product_id = btnBuy.getAttribute("data-id");
        fastOrder(product_id);
    });
}

// Модалка просмотра покупки

const viewPurchase = (user_id, order_id, order_status) => {
    axios
        .get("api/user/order-detail", {
            params: {
                user_id: user_id,
                order_id: order_id,
            },
        })
        .then((response) => {
            const products = response.data.details;

            Swal.fire({
                title: "Просмотр покупки",
                customClass: {
                    title: "modal-title",
                    confirmButton: "confirm-button btn",
                },
                width: "850px",
                confirmButtonText: "Ок",
                showConfirmButton: false,
                showCloseButton: true,
                html:
                    "<table class='table-modal'>" +
                    "<thead>" +
                    "<tr class='table-head'>" +
                    "<th>Фото</th>" +
                    "<th>Название</th>" +
                    "<th>Цена</th>" +
                    "<th>Количество</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody class='table-body'>" +
                    // `${productsInner}` +
                    "</tbody>" +
                    "</table>" +
                    "<div class='table-status'>" +
                    "<div class='delivery-status'>Доставлен</div>" +
                    `<div class='status-cost'>Итого: 7111 тг </div>` +
                    "</div>",
            });
            const tableBody = document.querySelector(".table-body");
            const productsInner = products.map((product) => {
                tableBody.innerHTML += `<tr class='table-item'>
                <td> <img src='${product.product.image}' class='table-img'</td>
                <td>${product.product.name}</td>
                <td><b>${product.full_price}</b></td>
                <td> ${product.quantity} шт. </td>
                </tr>`;
            });

            const deliveryStatus = document.querySelector(".delivery-status");
            deliveryStatus.innerHTML = `${order_status}`;
            const costFull = document.querySelector(".status-cost");
            const fullPrice = [];
            const productPrices = products.map((product) =>
                fullPrice.push(product.price)
            );
            costFull.innerHTML = `Итого: ${fullPrice.reduce(
                (prev, next) => prev + next
            )}`;
        });
};

const purchasesBtn = document.querySelectorAll(".purchases-btn");

purchasesBtn.forEach((item) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        let user_id = e.target.getAttribute("data-user");
        let order_id = e.target.getAttribute("data-id");
        let order_status = e.target.getAttribute("data-orderStatus");
        viewPurchase(user_id, order_id, order_status);
    });
});
