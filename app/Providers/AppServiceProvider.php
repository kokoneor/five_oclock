<?php

namespace App\Providers;

use App\Models\AboutFooter;
use App\Models\Catalog;
use App\Models\Contact;
use App\Models\SocialNetwork;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $about_footer = AboutFooter::query()->first();
            $social_networks = SocialNetwork::query()->get();
            $contacts = Contact::query()->first();
            $categories = Catalog::query()->where('catalog_id', '=', null)->get()->chunk(2);


            $view->with('categories', $categories);
            $view->with('about_footer', $about_footer);
            $view->with('social_networks', $social_networks);
            $view->with('contacts', $contacts);
        });
        //
    }
}
