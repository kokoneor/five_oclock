<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class SpecialOrder extends Model
{
    use HasFactory;

    protected $table    = 'orders';

    protected $fillable = [
        'user_id',
        'username',
        'surname',
        'email',
        'phone',
        'order_type',
        'delivery_type',
        'address',
        'building',
        'apartment',
        'city',
        'country',
        'postal_code',
        'message',
        'status',
        'package',
        'full_price',
        'order_date',
    ];
}
