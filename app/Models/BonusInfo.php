<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class BonusInfo extends Model
{
    use HasFactory;

    protected $table    = 'bonus_info';
}
