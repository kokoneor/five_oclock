<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;


class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
        'full_price'
    ];

    public function product()
    {
        return $this->hasOne(
            Product::class,
                'id',
                'product_id'
        );
    }
   
}
