<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partial extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'value', 'ingredient', 'dimension', 'product_id'
    ];

}
