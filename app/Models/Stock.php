<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;

    public function getPromoUser($user_id, $promo_id)
    {
        if($user_id){
            $promoUser = StockUser::query()
                ->where('user_id', $user_id)
                ->where('stock_id', $promo_id)
                ->first();
            if($promoUser){
                return true;
            }
        }

        return false;
    }
}
