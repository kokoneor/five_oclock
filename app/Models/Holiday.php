<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Holiday extends Model
{
    public function products()
    {
        return $this->hasMany(HolidayProduct::class);
    }

    public function cakes()
    {
        return $this->hasMany(HolidayCake::class);
    }
}
