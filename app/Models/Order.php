<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'surname',
        'email',
        'phone',
        'order_type',
        'delivery_type',
        'address',
        'building',
        'apartment',
        'city',
        'country',
        'postal_code',
        'message',
        'status',
        'package',
        'full_price',
        'order_date',
        'paid_with_bonuses',
        'pay_out_bonus',
        'return_bonus',
        'bonus_from_order',
        'status_id',
    ];

    public function statuses()
    {
        return $this->hasOne(
            OrderStatus::class,
            'id',
            'status_id'
        );
    }
}
