<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class MainTab extends Model
{
    use HasFactory;

    public function getProducts()
    {
        $products = [];
        $ids = TabProduct
            ::query()
            ->where('main_tab_id', $this->id)
            ->select('product_id')
            ->get()
            ->pluck('product_id');
        if($ids) {
            $products = Product::query()
                ->whereIn('id', $ids)
                ->get();
        }
        return $products;
    }
}
