<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->hasOne(
            Catalog::class,
            'id',
            'catalog_id'
        );
    }

    public function getIngredients()
    {
        $ingredients = [];
        $ids = ProductIngredient
            ::query()
            ->where('product_id', $this->id)
            ->select('ingredient_id')
            ->get()
            ->pluck('ingredient_id');
        if($ids) {
            $ingredients = Ingredient::query()
                ->whereIn('id', $ids)
                ->get();
        }
        return $ingredients;
    }

    public function partials()
    {
        return $this->hasMany(Partial::class);
    }

    public function variations()
    {
        return $this->hasMany(Variation::class);
    }


}
