<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class UserBonus extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'bonus_id',
        'bonus',
        'sum',
        'balance',
        'total_sum'
    ];

    public function getBonusInfo()
    {
        return Bonus::query()->where('id', $this->bonus_id)->first();
    }
}
