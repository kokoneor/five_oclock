<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'image'             => 'storage/' . $this->image,
            'category_id'       => $this->category_id,
            'is_main'           => $this->is_main,
            'parent_id'         => $this->parent_id,
        ];
    }
}
