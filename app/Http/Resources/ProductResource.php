<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'price'             => $this->price,
            'description'       => $this->description,
            'image'             => 'storage/' . $this->image,
            'keywords'          => $this->keywords,
            'catalog_id'        => $this->category,
            'is_active'         => $this->is_active,
            'created_at'        => $this->created_at,
        ];
    }
}
