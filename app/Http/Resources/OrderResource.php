<?php

namespace App\Http\Resources;

use App\Models\OrderReview;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $list = [];
        foreach ($this->details as $key => $detail) {
            $list[$key]['id']               = $detail->product->id;
            $list[$key]['title']            = $detail->product->title;
            $list[$key]['description']      = $detail->product->description;
            $list[$key]['image']            = 'storage/' . $detail->product->image;
            $list[$key]['category_id']      = $detail->product->category_id;
            $list[$key]['quantity']         = $detail->quantity;
            $list[$key]['price']            = $detail->price;
            $list[$key]['full_price']       = $detail->full_price;
        }

        $review     = OrderReview::where('user_id', $this->user_id)
            ->where('order_id', $this->id)
            ->first();
        if(!is_null($review)){
            $statusReview   = $review->status;
        }else{
            $statusReview   = $this->status_id == 2 ? 1 : 0;
        }

        return [
            'id'                => $this->id,
            'order_date'        => $this->created_at->format('d.m.Y'),
            'review'            => $statusReview,
            'full_price'        => $this->getFullPrice(),
            'status'            => $this->status->title,
            'list'              => $list
        ];
    }
}
