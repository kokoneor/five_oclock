<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\BonusInfo;
use App\Models\Catalog;
use App\Models\Catering;
use App\Models\Contact;
use App\Models\Event;
use App\Models\FilterPrice;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\ProductIngredient;
use App\Models\Stock;
use Carbon\Carbon;
use Doctrine\DBAL\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function sales()
    {
        $model = Stock::query()->first();
        $user_id = Auth::user() ? Auth::id() : null;
        $promoUser = $model->getPromoUser($user_id, $model->id);


        return view('pages.promo', compact('model', 'user_id', 'promoUser'));
    }

    public function sale($id)
    {
        $model = Stock::query()->where('id', $id)->first();
        $user_id = Auth::user() ? Auth::id() : null;
        $promoUser = $model->getPromoUser($user_id, $model->id);

        return view('pages.promo', compact('model', 'user_id', 'promoUser'));
    }

    public function about()
    {
        $model = About
            ::query()
            ->first()
        ;

        $bonus_info = BonusInfo::query()->get();


        return view('pages.about', compact('model', 'bonus_info'));
    }

    public function contacts()
    {
        $model = Contact::query()->first();

        return view('pages.contacts', compact('model'));
    }

    public function events()
    {
        $event = Event::query()->first();

        return view('pages.event', compact('event'));
    }

    public function event($id)
    {
        $event = Event::query()->where('id', $id)->first();

        return view('pages.event', compact('event'));
    }

    public function catering()
    {
        $catering = Catering::query()->first();
        $events = Event::query()->get();

        return view('pages.catering', compact('catering', 'events'));
    }

    public function cart()
    {
        $user = Auth::user();

        return view('pages.cart', compact('user'));
    }

    public function catalog()
    {
        $catalog = Catalog::query()->get();
        $filter_prices = FilterPrice::query()->get();
//        dd($catalog);

        return view('pages.catalog', compact('catalog', 'filter_prices'));
    }

    public function itemCard($id)
    {
        $product = Product::with('partials', 'variations')->find($id);
        $partials = $product->partials;
//        $product->ingredients = $product->getIngredients();
        $variations = $product->variations;
//        dd($variations);


        $similar_products = Product::query()
            ->where('catalog_id', $product->catalog_id)
            ->where('id', '!=', $id)
            ->get();


        return view('pages.cardItem', compact('product', 'similar_products', 'partials', 'variations'));
    }

    public function test()
    {
        return view('pages.test');
    }
}
