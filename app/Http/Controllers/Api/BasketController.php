<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class BasketController extends Controller
{
    public function index(Request $request)
    {
        $products   = collect($request->all());
        $ids = $products->pluck('id');

        $response = Product
            ::query()
            ->whereIn('id', $ids)
            ->select('id', 'name', 'image', 'price')
            ->get()
        ;

        foreach ($response as $product) {
            $product->quantity = $products->where('id', $product->id)->first()['quantity'];
        }

        return response(
            [
                'products'   => $response,
            ], Response::HTTP_OK
        );
    }
}
