<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class IngredientController extends Controller
{
    public function get()
    {
        return Ingredient::query()->select('id', 'name', 'description', 'weight')->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name"              => "required|string",
            "weight"            => "required|string"
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $ingredient  = Ingredient::create([
                'name'              => $request->name,
                'weight'             => $request->weight,
            ]);

            return response([
                'message' => 'Ингредиент создан!'
            ], 200);
        }
    }

    public function show($id)
    {
        return Ingredient::query()
            ->where('id', $id)
            ->select('id', 'name', 'description', 'weight')
            ->first();
    }
}
