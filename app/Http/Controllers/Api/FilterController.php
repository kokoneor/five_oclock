<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\Product;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FilterController extends Controller
{
    public function getProducts(Request $request)
    {
        $products = Product::query()->with('partials', 'variations');

        $categories = [];
        $prices = [];
        $sort = '';

        foreach ($request->all() as $filter) {
            if($filter['type'] == 'category') {
                $categories[] = $filter;
            }
            if($filter['type'] == 'price') {
                $prices[] = $filter;
            }
            if($filter['type'] == 'sort'){
                $sort = $filter['range'];
            }
        }

        $products->where(function ($query) use ($categories) {
            foreach ($categories as $category) {
                $query->orWhere('catalog_id', $category['id']);
            }
        });

        $products->where(function ($query) use ($prices) {
            foreach ($prices as $price) {
                $query->orWhereBetween('price', explode('-', $price['range']));
            }
        });

        if($sort) {
            $products->orderBy('price', $sort);
        }

        $products = $products->paginate();

        $products->getCollection()
            ->transform(
                function ($product) {
                    $product->ingredients = $product->partials;
                    return $product;
                });

        return response([
            'products' => $products
        ], Response::HTTP_OK);
    }
}
