<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Bonus;
use App\Models\Catalog;
use App\Models\Holiday;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use App\Models\UserBonus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;


class OrderController extends Controller
{
    public function getSpecialProduct()
    {
        $holidays = Holiday::query()->get();
        $catalog = Catalog::query()->where('is_special', 1)->first();
        $products = $catalog->products;

        return response(
            [
                'products'   => $products,
                'holidays'  => $holidays
            ], Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "user_id"           => "nullable|integer",
            "order_type"        => "required|integer",
            "delivery_type"     => "required|integer",
            "username"          => "required|string",
            "surname"           => "nullable|string",
            "phone"             => "nullable|string",
            "email"             => "nullable|string",
            "address"           => "nullable|string",
            "building"          => "nullable|string",
            "apartment"         => "nullable|string",
            "city"              => "nullable|string",
            "country"           => "nullable|string",
            "postal_code"       => "nullable|string",
            "message"           => "nullable|string",
            "full_price"        => "nullable|integer",
            "package"           => "nullable|integer",
            "user_balance"      => "nullable|integer",
            'products'          => 'nullable|json'
        ]);


        $order  = Order::create([
            'user_id'           => $request->user_id ? $request->user_id : null,
            'status'            => 0,
            'order_type'        => 3,
            'delivery_type'     => $request->delivery_type ? $request->delivery_type : null,
            'username'          => $request->username ? $request->username : null,
            'surname'           => $request->surname ? $request->surname : null,
            'phone'             => $request->phone ? $request->phone : null,
            'email'             => $request->email ? $request->email : null,
            'address'           => $request->address ? $request->address : null,
            'building'          => $request->building ? $request->building : null,
            'apartment'         => $request->apartment ? $request->apartment : null,
            'city'              => $request->city ? $request->city : null,
            'country'           => $request->country ? $request->country : null,
            'postal_code'       => $request->postal_code ? $request->postal_code : null,
            'full_price'        => $request->full_price ? $request->full_price : null,
            'message'           => $request->message ? $request->message : null,
            'paid_with_bonuses' => $request->user_balance,
        ]);

        $userBonus = UserBonus::query()->where('user_id', $request->user_id)->first();
        if($userBonus){
            $balance = $userBonus->balance - $request->user_balance;
            $userBonus->update(['balance' => $balance]);
        }

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            if(!is_null($request->products)) {

                $products = json_decode($request->products);

                foreach ($products as $product) {
                    OrderDetail::create([
                        'order_id'          => $order->id,
                        'product_id'        => $product->id,
                        'quantity'          => $product->quantity,
                        'price'             => $product->price,
                        'full_price'        => $product->quantity * $product->price,
                    ]);

                }
            }

        }

        return response([
            'message' => 'Заказ принят в обработку!',
            'url'       => '/profile',
        ], 200);

    }

    public function specialStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "username"          => "required|string",
            "phone"             => "required|string",
            "address"           => "required|string",
            "message"           => "nullable|string",
            "holiday_id"        => "nullable|integer",
            'product_id'        => 'required|integer'
        ]);


        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $product = Product
                ::query()
                ->where('id', $request->product_id)
                ->select('id', 'name', 'image', 'price')
                ->first()
            ;

            $order  = Order::create([
                'user_id'           => Auth::user() ? Auth::id() : null,
                'status'            => 1,
                'order_type'        => 2,
                'username'          => $request->username ? $request->username : null,
                'phone'             => $request->phone ? $request->phone : null,
                'address'           => $request->address ? $request->address : null,
                'full_price'        => null,
                'message'           => $request->message ? $request->message : null,
                'holiday_id'        => $request->holiday_id ? $request->holiday_id : null,
            ]);


            OrderDetail::create([
                'order_id'          => $order->id,
                'product_id'        => $product->id,
                'quantity'          => 1,
                'price'             => $product->price,
                'full_price'        => $product->price,
            ]);

            return response([
                'message' => 'Заказ принят в обработку!'
            ], 200);
        }

    }

    public function fastOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "username"          => "required|string",
            "phone"             => "required|string",
            "address"           => "required|string",
            "product_id"        => "required|integer",
            "quantity"          => "required|integer",
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $product = Product::query()->where('id', $request->product_id)->first();

            $order  = Order::create([
                'user_id'           => Auth::user() ? Auth::id() : null,
                'status'            => 1,
                'order_type'        => 1,
                'username'          => $request->username ? $request->username : null,
                'phone'             => $request->phone ? $request->phone : null,
                'address'           => $request->address ? $request->address : null,
                'full_price'        => $product->price * $request->quantity,
                'holiday_id'        => $request->holiday_id ? $request->holiday_id : null,
            ]);


            $orderDetail = OrderDetail::create([
                    'order_id'          => $order->id,
                    'product_id'        => $product->id,
                    'quantity'          => $request->quantity,
                    'price'             => $product->price,
                    'full_price'        => $product->price * $request->quantity,
                ]);

            return response([
                'message' => 'Заказ принят в обработку!'
            ], 200);
        }

    }

    public function toOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "username"          => "required|string",
            "phone"             => "required|string",
            "address"           => "required|string",
            "message"           => "nullable|string",
            "order_date"        => "nullable|date",
            "product_id"        => "required|integer",
            "quantity"          => "required|integer",
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $product = Product::query()->where('id', $request->product_id)->first();

            $order  = Order::create([
                'user_id'           => Auth::user() ? Auth::id() : null,
                'status'            => 1,
                'order_type'        => 4,
                'username'          => $request->username,
                'phone'             => $request->phone,
                'address'           => $request->address,
                'message'           => $request->message,
                'order_date'        => $request->order_date,
                'full_price'        => $product->price * $request->quantity,
            ]);


            $orderDetail = OrderDetail::create([
                'order_id'          => $order->id,
                'product_id'        => $product->id,
                'quantity'          => $request->quantity,
                'price'             => $product->price,
                'full_price'        => $product->price * $request->quantity,
            ]);

            return response([
                'message' => 'Заказ принят в обработку!'
            ], 200);
        }

    }

    public function endOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id"        => "required|integer"
        ]);

        $order = Order::query()->where('id', $request->id)->first();

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else {
            if (!is_null($order) && !is_null($order->user_id)) {

                $userBonus = UserBonus::query()->where('user_id', $order->user_id)->first();

                if($userBonus){
                    $balance = $this->calculationBalance($userBonus->id, $order);
                }else{
                    $bonus = UserBonus::create([
                        'user_id'           => $order->user_id,
                        'bonus_id'          => null,
                        'bonus'             => 0,
                        'balance'           => 0,
                        'sum'               => 0,
                        'total_sum'         => 0
                    ]);
                    $balance = $this->calculationBalance($bonus->id, $order);
                }

                $order->update([
                    'pay_out_bonus' => 1,
                    'bonus_from_order' => $balance,
                    'return_bonus' => 1
                ]);

                return response(['message' => 'Заказ завершен!']);
            }
        }

    }

    public function calculationBalance($userBonusId, $order)
    {
        $order_price = $order->full_price;
        $userBonus = UserBonus::query()->where('id', $userBonusId)->first();

        if($userBonus->bonus === 0){
            $bonus = Bonus::query()->orderBy('id', 'ASC')->first();
            $userBonus->bonus_id = $bonus->id;
            $userBonus->bonus = $bonus->bonus;
            $userBonus->sum = $order_price;
            $userBonus->total_sum = $order_price;

            $user_balance = ((int)$order_price * 10) / 100;
            $userBonus->balance = $user_balance;
        }else{
            $bonus = Bonus::query()->where('id', $userBonus->bonus_id)->first();
            $user_sum = $userBonus->sum + $order_price;
            $user_total_sum = $userBonus->total_sum + $order_price;
            $user_balance = $userBonus->balance + (($order_price * $bonus->bonus) / 100);

            $userBonus->balance = $user_balance;

            if($user_sum > $bonus->sum){
                $nextBonus = Bonus::query()
                    ->where('id', '>', $bonus->id)
                    ->oldest('id')
                    ->first();

                $userBonus->bonus_id = $nextBonus ? $nextBonus->id : $bonus->id;
                $userBonus->bonus = $nextBonus ? $nextBonus->bonus : $bonus->bonus;
                $userBonus->sum = $user_sum - $bonus->sum;
                $userBonus->total_sum = $user_total_sum;
            }else{
                $userBonus->bonus_id =$bonus->id;
                $userBonus->bonus = $bonus->bonus;
                $userBonus->sum = $user_sum - $bonus->sum;
                $userBonus->total_sum = $user_total_sum;
            }
        }

        if($userBonus->update()){
            return $user_balance;
        }

        return 0;
    }

}
