<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductController extends Controller
{
    public function get()
    {
        return ProductResource::collection(
            Product
                ::query()
                ->get()
        );
    }

    public function show($id)
    {
        return Product::query()
            ->where('id', $id)
            ->select('id', 'name', 'description', 'price', 'image')
            ->first();
    }
}