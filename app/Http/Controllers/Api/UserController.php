<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        dd(Auth::user());
        $user = User::query()->where('id', Auth::id())->first();
        $bonus = $user->userBonus;

        if($user){
            if(is_null($bonus)){
                $bonus = null;
            }
            return response()->json(
                [
                    'user' => $user,
                    'bonus' => $bonus
                ]
            );
        }else{
            return response()->json(
                [
                    'user' => null
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "user_id"           => "required|integer",
            "name"              => "required|string",
            "surname"           => "nullable|string",
            "birthday"          => "nullable|date",
        ]);


        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $user = User::query()->where('id', $request->user_id)->first();

            $user->update([
                'name'              => $request->name,
                'surname'           => $request->surname,
                'birthday'          => $request->birthday ? $request->birthday : null,
            ]);


            return response([
                'message' => 'Данные пользователя обновлен!'
            ], 200);
        }

    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required|integer',
            'password'  => 'required|string|min:6|confirmed',
        ]);


        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->first()], 422);
        }else{

            $user = User::query()->where('id', $request->user_id)->first();
            $user->update([
                'password' => Hash::make($request->password),
            ]);


            return response([
                'message' => 'Пароль пользователя обновлен!'
            ], 200);
        }

    }

    public function orderDetail(Request $request)
    {
        $order = Order::query()
            ->where('id', $request->order_id)
            ->first();

        if($order){
            $orderDetails = OrderDetail::query()
                ->where('order_id', $order->id)
                ->get();
            $response = [];
            foreach($orderDetails as $detail){
                $response[] = [
                    'order_id'  => $detail->order_id,
                    'product'   => [
                        'name'  => $detail->product->name,
                        'image' => 'storage/' . $detail->product->image,
                    ],
                    'price'     => $detail->price,
                    'quantity'  => $detail->quantity,
                    'full_price'    => $detail->full_price
                ];
            }
            return response([
                'details' => $response
            ], 200);
        }else{
            return response([
                'message' => 'Заказ не найден!'
            ], 200);
        }

    }

    public function user(Request $request)
    {
        $user = $request
            ->user();

        return response()->json($user);
    }

    public function show(Request $request)
    {
        return response()->json(
            [
                'user' => $request->user(),
            ]
        );
    }

}
