<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Stock;
use App\Models\StockUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;


class PromoController extends Controller
{
    public function takePart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promo_id'          => 'required|integer',
            'user_id'           => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }else{

            $promoUser = StockUser::query()
                ->where('user_id', $request->user_id)
                ->where('stock_id', $request->promo_id)
                ->first();

            if($promoUser){

                return response([
                    'message' => 'Вы уже приняли в участие акции!'
                ], 200);
            }else {

                $promoUser = StockUser::create([
                    'stock_id' => $request->promo_id,
                    'user_id' => $request->user_id
                ]);

                return response([
                    'message' => 'Вы приняли в участие акции!'
                ], 200);
            }
        }

    }

}
