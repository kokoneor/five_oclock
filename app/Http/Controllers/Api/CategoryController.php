<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Models\Catalog;

class CategoryController extends Controller
{
    public function get()
    {
        return CategoryResource::collection(
            Catalog
                ::query()
                ->get()
        );
    }

    public function products(Catalog $category)
    {
        return ProductResource::collection($category->products);
    }
}