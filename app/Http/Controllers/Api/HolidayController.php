<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Holiday;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class HolidayController extends Controller
{
    public function getHolidayProducts(Request $request)
    {
        $holiday = Holiday::query()->where('id', $request->holiday_id)->first();
        $cakesId = [];
        $productsId = [];
        foreach ($holiday->cakes as $cake){
            $cakesId[] = $cake->product_id;
        }
        foreach ($holiday->products as $product){
            $productsId[] = $product->product_id;
        }
        $cakes = ProductResource::collection(Product::query()->whereIn('id', $cakesId)->get());
        $products = ProductResource::collection(Product::query()->whereIn('id', $productsId)->get());

        return response(
            [
                'cakes' => $cakes,
                'products' => $products
            ], Response::HTTP_OK
        );
    }

}
