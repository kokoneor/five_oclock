<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $user = User::query()->where('id', Auth::id())->first();
        $bonus = $user->userBonus;
        $orders = Order::query()->where('user_id', $user->id)->get();

        return view('profile.index', compact('user', 'bonus', 'orders'));
    }

    public function info()
    {
        $user = User::query()->where('id', Auth::id())->first();
        $bonus = $user->userBonus;

        if($user){
            if(is_null($bonus)){
                $bonus = null;
            }
            return response()->json(
                [
                    'user' => $user,
                    'bonus' => $bonus
                ]
            );
        }else{
            return response()->json(
                [
                    'user' => null
                ]
            );
        }
    }

}
