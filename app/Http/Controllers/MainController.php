<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Catalog;
use App\Models\Event;
use App\Models\MainTab;
use App\Models\Product;
use App\Models\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function home()
    {
        $banners = Banner::query()->get();
        $catalog = Catalog::query()->where('is_main', 1)->get();
        $new_products = Product::query()->with('partials', 'variations')->where('is_new', 1)->get();

        foreach ($new_products as $product){
            $product->ingredients = $product->getIngredients();
        }

        $events = Event::query()->get();
        $sales = Stock::query()->where('is_main', 1)->get();
        $mainTabs = MainTab::query()->get();


        return view('pages.index', compact('banners', 'catalog', 'new_products',
            'events', 'sales', 'mainTabs'));
    }

    public function pdf()
    {
        return view('pages.pdf');
    }

}
