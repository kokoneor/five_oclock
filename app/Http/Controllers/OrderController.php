<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class OrderController extends Controller
{

    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['integer'],
            'username' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10'],
            'product_id' => ['required', 'integer'],
            'quantity' => ['required', 'integer'],
        ]);

        $order  = Order::create([
            'user_id'           => Auth::user() ? Auth::id() : null,
            'status_id'         => 1,
            'order_type'        => 2,
            'username'          => $request->username ? $request->username : null,
            'phone'             => $request->phone ? $request->phone : null,
            'address'           => $request->address ? $request->address : null,
        ]);
        $product = Product::query()->where('id', $request->product_id)->first();

        $order_detail = OrderDetail::create([
            'order_id'          => $order->id,
            'product_id'        => $request->product_id ? $request->product_id : null,
            'quantity'          => $request->quantity ? $request->quantity : null,
            'price'             => $product ? $product->price : null,
            'full_price'        => $product ? $product->price * $request->quantity : null,
        ]);

        return $order_detail;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'integer',
            'username' => 'required|string|max:255',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'product_id' => 'required|integer',
            'quantity' => 'required|integer',
        ]);

        $order  = Order::create([
            'user_id'           => Auth::user() ? Auth::id() : null,
            'status_id'         => 1,
            'order_type'        => 2,
            'username'          => $request->username ? $request->username : null,
            'phone'             => $request->phone ? $request->phone : null,
            'address'           => $request->address ? $request->address : null,
        ]);
        $product = Product::query()->where('id', $request->product_id)->first();

        $order_detail = OrderDetail::create([
            'order_id'          => $order->id,
            'product_id'        => $request->product_id ? $request->product_id : null,
            'quantity'          => $request->quantity ? $request->quantity : null,
            'price'             => $product ? $product->price : null,
            'full_price'        => $product ? $product->price * $request->quantity : null,
        ]);
    }


}