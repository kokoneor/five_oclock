<?php


namespace App\Traits;


use App\Http\Resources\Product\DimensionResource;
use App\Http\Resources\Product\IngreResource;
use App\Models\Dimension;
use App\Models\Ingredient;

trait ProductTrait
{
    public static function partials($partials)
    {
        $partials = json_decode($partials);
        $partialList = [];
        foreach ($partials as $partial){
            $partialList[] =[
                'ingredient' => new IngreResource(Ingredient::find($partial->ingredient_id)),
                'dimension' => new DimensionResource(Dimension::find($partial->dimension_id)),
                'value' => $partial->value
            ];
        }
        return $partialList;
    }
}
