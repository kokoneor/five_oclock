<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\FilterController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\HolidayController;
use App\Http\Controllers\Api\PromoController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\IngredientController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BasketController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/category/list', [CategoryController::class, 'get']);
Route::get('/category/products', [CategoryController::class, 'products']);
Route::get('/products/list', [ProductController::class, 'get']);
Route::post('/cart/data', [BasketController::class, 'index']);

Route::get('/product/show/{id}', [ProductController::class, 'show']);
Route::get('/ingredients', [IngredientController::class, 'get']);

Route::post('/filter/products', [FilterController::class, 'getProducts']);

Route::get('/holiday/products', [HolidayController::class, 'getHolidayProducts']);
Route::get('/order/special-product', [OrderController::class, 'getSpecialProduct']);
Route::post('/order/status', [OrderController::class, 'endOrder']);
Route::post('/order/store', [OrderController::class, 'store']);
Route::post('/order/special', [OrderController::class, 'specialStore']);
Route::post('/order/fast', [OrderController::class, 'fastOrder']);
Route::post('/order/to-order', [OrderController::class, 'toOrder']);

Route::post('/promo/take-part', [PromoController::class, 'takePart']);

Route::get('/user/info', [UserController::class, 'index']);
Route::post('/user/update', [UserController::class, 'update']);
Route::post('/user/update-password', [UserController::class, 'updatePassword']);
Route::get('/user/order-detail', [UserController::class, 'orderDetail']);

Route::namespace('Api')->group(function () {
    // Auth
//    Route::post('register', 'AuthController@login');
//    Route::post('login', 'AuthController@register');

    Route::middleware('auth')
        ->group(function () {
            Route::post('logout', 'LogoutController@logout');
            Route::prefix('users')
                ->group(function () {
                    Route::get('/', 'UserController@user');
                    Route::put('/', 'UserController@update');
                    Route::get('orders', 'UserController@orders');
                    Route::get('orders/{order}', 'UserController@order');
                    Route::get('favourites', 'UserController@favourites');
                });
        });
    // END Auth

});

Route::middleware('auth:api')
    ->group(function () {
        Route::post('logout', 'AuthController@logout');
        Route::prefix('users')
            ->group(function () {
                Route::get('/', [UserController::class, 'user']);
            });
    });
// END Auth


