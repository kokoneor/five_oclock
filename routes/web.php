<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Voyager\OrdersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/', [MainController::class, 'home'])->name('index');
Route::get('/menu', [MainController::class, 'pdf'])->name('pdf');
Route::get('/about', [PagesController::class, 'about'])->name('about');
Route::get('/catering', [PagesController::class, 'catering'])->name('catering');
Route::get('/events', [PagesController::class, 'events'])->name('events');
Route::get('/cart', [PagesController::class, 'cart'])->name('cart');
Route::get('/catalog', [PagesController::class, 'catalog'])->name('catalog');
Route::get('/sales', [PagesController::class, 'sales'])->name('sales');
Route::get('/item-card', [PagesController::class, 'itemCard'])->name('item-card');
Route::get('/contacts', [PagesController::class, 'contacts'])->name('contacts');
Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
Route::get('/profile/info', [ProfileController::class, 'info'])->name('info');

Route::get('/product/{id}', [PagesController::class, 'itemCard'])->name('item-card');
Route::get('/event/{id}', [PagesController::class, 'event'])->name('event');
Route::get('/sale/{id}', [PagesController::class, 'sale'])->name('sale');

Route::get('/test', [PagesController::class, 'test'])->name('test');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/home', [MainController::class, 'home'])->name('home');
Route::post('login', [LoginController::class, 'login']);
Route::post('register', [RegisterController::class, 'register']);
Route::post('logout', [AuthController::class, 'logout']);

Route::post('order-fast', [OrderController::class, 'fastOrder']);
