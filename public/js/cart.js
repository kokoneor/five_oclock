let totalCart = 0;
let deliveryPrice = 0;
let totalPrice = 0;
let promo = 0;
let cashback = 0;

function renderHeaderCounter() {
    Array.from(document.getElementsByClassName('cart-counter-by-hikaro'))
        .forEach(node => node.innerText = getProducts().length)
}

function addToCart(id, quantity) {
    let products = getProducts();
    let current = products.some(product => product.id == id)
    if(current) {
        products.find(product => product.id == id).quantity += quantity
        console.log(products)

    }
    else {
        products.push({
            id: id,
            quantity : quantity
        })
    }
    Swal.fire({
        icon: 'success',
        text: 'Продукт добавлен в корзину!'
    });
    localStorage.setItem('products', JSON.stringify(products))
    renderHeaderCounter()
}

function deleteFromCart(id) {
    let products = getProducts();
    let index = products.findIndex(product => product.id == id)
    products.splice(index, 1)
    localStorage.setItem('products', JSON.stringify(products))
    setTotalCart()
    setTotalPrice()
    document.getElementById('product-' + id).remove()
    renderHeaderCounter();
}

function getProducts() {
    if(!localStorage.getItem('products')){
        localStorage.setItem('products', JSON.stringify([]))
    }
    return JSON.parse(localStorage.getItem('products'))
}

function renderCartItem(product) {
    if(document.getElementById('product-' + product.id)) {
        document.getElementById('product-' + product.id).remove()
    }
    let item = `
    <div class="cart-item" id="product-${product.id}">
        <div class="cart-item_info">
            <img src="/storage/${product.image}" alt="Товар">
            <div class="cart-item_info-text">
                <h3>${product.name}</h3>
                <span onclick="deleteFromCart(${product.id})" class="cart-item_delete"><i class="icon-clear"></i> Удалить</span>
            </div>
        </div>
        <div>
            <div class="cart-count-control">
                <div class="count-control">
                    <button class="btn btn-yellow btn-count-modal minus" onclick="minus(${product.id})">-</button>
                    <span class="total-modal" id="product-${product.id}-quantity">${product.quantity}</span>
                    <button class="btn btn-count-modal plus" onclick="plus(${product.id})">+</button>
                </div>
                <h3 class="cart-item_cost" id="product-${product.id}-total">${product.price * product.quantity}</h3>
            </div>
        </div>
    </div>
    `
    document.getElementById('cart-items').innerHTML += item
}

async function getProductsFromAPI() {
    if(getProducts().length === 0){
        document.getElementById('cartTotal').style.display = 'none';
    }else {
        let data = (await axios.post('/api/cart/data', getProducts())).data.products;
        data.forEach(product => {
            renderCartItem(product)
        });
        localStorage.setItem('products', JSON.stringify(data));
        setTotalCart();
        setTotalPrice()
    }
}

function plus(id) {
    let products = getProducts();
    products.find(product => product.id == id).quantity += 1;
    localStorage.setItem('products', JSON.stringify(products));
    document.getElementById('product-' + id + '-total').innerText = products.find(product => product.id == id).quantity * products.find(product => product.id == id).price;
    document.getElementById('product-' + id + '-quantity').innerText = products.find(product => product.id == id).quantity;
    setTotalCart();
    setTotalPrice();
}

function minus(id) {
    let products = getProducts()
    let quantity = products.find(product => product.id == id).quantity;
    let price = products.find(product => product.id == id).price;
    if(quantity > 1) {
        products.find(product => product.id == id).quantity -= 1;
        localStorage.setItem('products', JSON.stringify(products));
        document.getElementById('product-' + id + '-total').innerText = products.find(product => product.id == id).quantity * products.find(product => product.id == id).price;
        document.getElementById('product-' + id + '-quantity').innerText = products.find(product => product.id == id).quantity;
        setTotalCart();
        setTotalPrice();
    }
}

function setTotalCart() {
    let products = getProducts()
    totalCart = products.reduce((accumulator, currentValue) => accumulator + currentValue.price * currentValue.quantity, 0);
    document.getElementById('total-cart').innerText = totalCart;
    setCashBack(totalCart);
}

function setTotalPrice() {
    let products = getProducts();
    let user = document.getElementById('userId');
    let checkboxBalance = document.getElementById('checkboxBalanceCart');
    setDeliveryPrice();
    setPromo();

    if(user.value) {
        if (checkboxBalance.checked) {
            totalPrice = products.reduce((accumulator, currentValue) => accumulator + currentValue.price * currentValue.quantity, 0);
            if(promo > totalPrice){
                document.getElementById('total-price').innerText = 0;
                document.getElementById('order_full_price').value = 0;
                document.getElementById('order_user_balance').value = promo - (promo - totalPrice);
            }else{
                document.getElementById('total-price').innerText = totalPrice + deliveryPrice - promo;
                document.getElementById('order_full_price').value = totalPrice + deliveryPrice - promo;
            }
            document.getElementById('order_products').value = localStorage.getItem('products');
        } else {
            totalPrice = products.reduce((accumulator, currentValue) => accumulator + currentValue.price * currentValue.quantity, 0);
            document.getElementById('total-price').innerText = totalPrice + deliveryPrice - promo;

            document.getElementById('order_full_price').value = totalPrice + deliveryPrice - promo;
            document.getElementById('order_products').value = localStorage.getItem('products');
        }
    }else{
        totalPrice = products.reduce((accumulator, currentValue) => accumulator + currentValue.price * currentValue.quantity, 0);
        document.getElementById('total-price').innerText = totalPrice + deliveryPrice - promo;

        document.getElementById('order_full_price').value = totalPrice + deliveryPrice - promo;
        document.getElementById('order_products').value = localStorage.getItem('products');
    }
}

function setCashBack(totalPrice) {
    let user = document.getElementById('userId');
    if(user.value) {
        cashback = (totalPrice * parseFloat(document.getElementById('userBonusCashBack').value)) / 100;
        document.getElementById('orderCashBack').innerText = cashback;
    }
}

function setPromo() {
    let user = document.getElementById('userId');
    if(user.value) {
        document.getElementById('in-access-user-balance').innerText = document.getElementById('userBonusBalance').value;
    }
}

function balanceUser() {
    let checkedBalance = document.getElementById('checkboxBalanceCart');
    if(document.getElementById('userBonusBalance').value === '0'){
        checkedBalance.checked = false;
    }

    if(checkedBalance.checked){
        promo = parseFloat(document.getElementById('userBonusBalance').value);
        document.getElementById('access-user-balance').innerText = promo;
        document.getElementById('order_user_balance').value = promo;
        setTotalPrice();
    }else{
        promo = 0;
        document.getElementById('access-user-balance').innerText = promo;
        document.getElementById('order_user_balance').value = promo;
        setTotalPrice();
    }
}

function setDeliveryPrice() {
    let checkbox = document.getElementById('deliveryCheckboxCart');

    if(checkbox.checked){
        deliveryPrice = parseInt(document.getElementById('deliveryPriceCart').value);
    }else{
        deliveryPrice = 0;
    }
}

function setDeliveryCart() {
    let deliveryBtn = document.getElementById("deliveryCheckboxCart");
    deliveryBtn.checked = true;
    let pickupBtn = document.getElementById("pickupCheckboxCart");
    pickupBtn.checked = false;

    document.getElementById('cartTotalDelivery').style.display = 'flex';
    document.querySelector(".delivery_form").style.display = "block";
    deliveryPrice = parseInt(document.getElementById('deliveryPriceCart').value);
    document.getElementById('order_type_cart').value = 1;

    setTotalPrice();
}

function setPickupCart() {
    let pickupBtn = document.getElementById("pickupCheckboxCart");
    pickupBtn.checked = true;
    let deliveryBtn = document.getElementById("deliveryCheckboxCart");
    deliveryBtn.checked = false;

    document.querySelector(".delivery_form").style.display = "none";
    deliveryPrice = 0;
    document.getElementById('cartTotalDelivery').style.display = 'none';
    document.getElementById('order_type_cart').value = 2;
    setTotalPrice();
}
