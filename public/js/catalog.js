let filters = [];

function composeFilters(filter, id) {
    let checkbox = document.getElementById(id)
    if(checkbox.checked) {
        filters.push(filter)
    }
    else {
        let index = filters.findIndex(f => f.id == filter.id && f.type == filter.type);
        // console.log(index);
        filters.splice(index, 1)
    }
    getFilterProductsFromAPI()
}

function composeSortFilter(filter){
    let index = filters.findIndex(f => f.type == filter.type);
    filters.splice(index, 1);

    filters.push(filter);
    getFilterProductsFromAPI()
    console.log(filter);
}

function clearFilter()
{
    filters = [];
    getFilterProductsFromAPI();
}

function renderProductItem(product) {
    let item = `
        <div class="card-item" id="product-${product.id}" style="height: 580px">
            <div class="card-item_content">
                <a href="/product/${product.id}">
                    <img src="/storage/${product.image}" alt="Название товара" class="card-item_img" style="height: 250px">
                    <h3 class="card-item_name">${product.name}</h3>
                    <div class="card-ingredients-block" style="height: 50px">`;

    if (product.ingredients.length !== 0) {
        item += `<span>Ингридиенты:</span>`;
    }
    item += `<div class="ingredients">`;

    product.ingredients.forEach(function (ingredient) {
        item += '<p>' + ingredient.ingredient + ' - ' + ingredient.dimension + '</p>'
    })
    item += `</div>
            </div>
                <div class="card-item_cost">`;
    if(product.dimension){
        item += `<span>Цена за 1 ${product.dimension}</span>`;
    }else{
        item += `<span>Цена за 1 кг</span>`;
    }
    item += `
                    <p>${product.price} тг</p>
                </div>
                </a>
            </div>
            <div class="card-item_buttons">
                <button class="btn card-btn btn-yellow" data-id="${product.id}">
                    <i class="icon-money"></i>Купить
                </button>
                <button class="btn card-btn-cart" onclick="addToCart(${product.id}, 1)"><i class="icon-cart"></i>В корзину</button>
            </div>
        </div>
    `
    document.getElementById('productsFilter').innerHTML += item;
}

async function getFilterProductsFromAPI()
{
    let response = (await axios.post('/api/filter/products', filters)).data;
    let products = response.products.data;
    console.log(filters);
    document.getElementById('productsFilter').innerHTML = '';
    products.forEach(product => {
        renderProductItem(product)
    });
    // console.log(products)

}

getFilterProductsFromAPI();
