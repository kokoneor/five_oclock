const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
    },
    buttonsStyling: false
});

$('#orderCartForm').on('submit', function (e) {
    e.preventDefault();
    if(document.getElementById('pickupCheckboxCart').checked){
        document.getElementById('delivery_type_cart').value = 2;
    }else{
        document.getElementById('delivery_type_cart').value = 1;
    }

    let form    = new FormData(document.forms.orderCartForm);

    axios.post('/api/order/store', form)
        .then(function (response) {
            console.log(response);
            Swal.fire({
                icon: 'success',
                text: response.data.message
            });
            // document.getElementById('orderCartForm').reset();
            // localStorage.removeItem('products');
        })
        .catch(function (error) {
            console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});

$('#profileUpdateForm').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.profileUpdateForm);

    axios.post('/api/user/update', form)
        .then(function (response) {
            console.log(response);
            Swal.fire({
                icon: 'success',
                text: response.data.message
            });
            window.location.href = "/profile";
        })
        .catch(function (error) {
            console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});

$('#updatePasswordForm').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.updatePasswordForm);

    axios.post('/api/user/update-password', form)
        .then(function (response) {
            console.log(response);
            Swal.fire({
                icon: 'success',
                text: response.data.message
            });
            window.location.href = "/profile";
        })
        .catch(function (error) {
            console.log(error.response)
            Swal.fire({
                icon: 'error',
                text: error.response.data.errors
            });
        });
});


$('#takePartBtn').on('submit', function (e) {
    e.preventDefault();
    let userId = document.getElementById('promoUserId').value;

    let form    = new FormData(document.forms.takePartBtn);

    if(userId){
        axios.post('/api/promo/take-part', form)
            .then(function (response) {
                console.log(response);
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                window.location.href = "/";
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    }else{
        alert('Войдите в свой аккаунт пожалуйста');
    }


});
